use hermsn;

mod server;

#[test]
fn it_starts_and_stops() {
    let ds = hermsn::NotificationServer::start(
        server::next_socket_v4(),
        &vec!["MSNP2".to_string()],
        "sqlite:hermsn.db".to_string(),
    );
    ds.stop();
}
