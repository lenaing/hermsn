use std::net::{Ipv4Addr, SocketAddrV4};

pub fn next_socket_v4() -> SocketAddrV4 {
    SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 0)
}
