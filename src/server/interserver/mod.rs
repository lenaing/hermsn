//! MSN Inter-Servers.
use crate::command::Command;
use crate::connection::Connection;
use crate::database::Database;
use crate::protocol::MSNP2InterServer;
use crate::protocol::Protocol;
use crate::server::{ClientList, IdentifiedClientList, InternalServer};

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

/// An MSN Notification Inter-Server.
#[derive(Clone)]
pub struct NotificationInterServer {
    /// Is this server exiting?
    pub exiting: Arc<AtomicBool>,
    /// Current client connections established on this server.
    pub all_clients: ClientList,
    /// Identified users on this Notification Server.
    pub users: IdentifiedClientList,
    /// Database connection pool.
    pub database: Arc<Database>,
}

impl InternalServer for NotificationInterServer {
    fn get_all_clients(&self) -> ClientList {
        Arc::clone(&self.all_clients)
    }

    fn is_exiting(&self) -> bool {
        self.exiting.load(Ordering::SeqCst)
    }

    fn stop(&self) {
        self.exiting.store(true, Ordering::SeqCst);

        // Force disconnect all currently connected clients.
        // Cf https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.10
        {
            let mut all_clients = self.all_clients.write().expect("Lock clients for write.");
            for (_, mut client) in all_clients.drain() {
                let _ = client.write("OUT SSD".to_string());
                client.close();
            }
        }
    }

    fn handle_client_command(
        &self,
        client: &mut Connection,
        dialect: &mut Option<Box<dyn Protocol>>,
        mut command: &mut Command,
    ) -> (bool, Option<Box<dyn Protocol>>) {
        match dialect {
            // An MSN Inter-Server Protocol dialect was previously selected.
            Some(dialect) => match dialect.handle(&mut command) {
                Ok(should_disconnect) => (should_disconnect, None),
                Err((_error, _tr_id, should_disconnect)) => (should_disconnect, None),
            },
            // No MSN Inter-Server Protocol dialect selected.
            _ => {
                // First parameter is the current MSN Protocol used by the client.
                let dialect = command.parameters.remove(0);

                // Use our own internal protocols for Inter-Server communications.
                let mut dialect: Box<dyn Protocol> = match dialect.as_str() {
                    "MSNP2" => Box::new(MSNP2InterServer::new(
                        client.clone(),
                        Arc::clone(&self.database),
                        Arc::clone(&self.users),
                    )),
                    _ => unimplemented!(),
                };

                // Convert command to a standard one.
                command.command_type = command.parameters.remove(0);
                match dialect.handle(&mut command) {
                    Ok(should_disconnect) => (should_disconnect, Some(dialect)),
                    Err((_error, _tr_id, should_disconnect)) => (should_disconnect, Some(dialect)),
                }
            }
        }
    }
}
