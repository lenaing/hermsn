//! MSN service servers components definition.
use crate::command::{Command, ParseCommandError};
use crate::connection::Connection;
use crate::database::Database;
use crate::protocol::{Protocol, ProtocolError, MSNP2};

use std::collections::HashMap;
use std::io;
use std::net::{SocketAddr, TcpListener};
use std::str::FromStr;
use std::sync::{Arc, RwLock};
use std::thread;
use std::time;

mod dispatch;
mod interserver;
mod notification;
mod switchboard;

pub use self::dispatch::DispatchServer;
pub use self::notification::NotificationServer;
pub use self::switchboard::SwitchboardServer;

/// A list of current MSN [`Connection`]s established on an MSN [Server][`InternalServer`].
type ClientList = Arc<RwLock<HashMap<String, Connection>>>;
/// A list of current MSN [`Connection`]s identified on an MSN [Server][`InternalServer`].
pub type IdentifiedClientList = Arc<RwLock<HashMap<String, Connection>>>;

/// An MSN [`InternalServer`] base definition.
///
/// This trait define common behaviors to all MSN [`InternalServer`]s.
trait InternalServer: Clone + Send + 'static {
    /// Returns all MSN Protocol dialects known to this server.
    fn get_dialects(&self) -> Option<Vec<String>> {
        None
    }

    /// Returns all current client connections established on this server.
    fn get_all_clients(&self) -> ClientList;

    /// Returns all current identified client connections established on this server.
    fn get_identified_clients(&self) -> Option<IdentifiedClientList> {
        None
    }

    /// Is this server exiting?
    fn is_exiting(&self) -> bool;

    /// Stop this server.
    fn stop(&self);

    /// Returns current database connection pool.
    fn get_database(&self) -> Option<Arc<Database>> {
        None
    }

    /// Is this server running?
    fn is_running(&self) -> bool {
        !self.is_exiting()
    }

    /// Create this server accept thread.
    ///
    /// # Arguments:
    /// * _addr_: TCP socket address to bind to.
    fn create_accept_thread(&self, addr: SocketAddr) -> thread::JoinHandle<()> {
        let server_clone = self.clone();

        // Spawn the "accept thread".
        // Its purpose is to listen for TCP connections and spawn
        // dedicated client handling threads per successful connection.
        thread::spawn(move || {
            let mut threads: Vec<thread::JoinHandle<()>> = Vec::new();
            let listener = TcpListener::bind(addr).unwrap();
            // Set listener to non-blocking so we can periodically check
            // for graceful shutdown.
            listener
                .set_nonblocking(true)
                .expect("cannot set non-blocking");
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        let server = server_clone.clone();
                        let mut client = Connection::new(&stream);

                        // Spawn dedicated client handling thread.
                        let handle = thread::spawn(move || {
                            let peer_addr = stream.peer_addr().unwrap().to_string();
                            let all_clients = server.get_all_clients();

                            {
                                all_clients
                                    .write()
                                    .unwrap()
                                    .insert(peer_addr.to_string(), client.clone());
                            }
                            server.handle_client(&mut client);
                            {
                                all_clients.write().unwrap().remove(&peer_addr);
                            }
                        });
                        threads.push(handle);
                    }
                    Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                        if server_clone.is_exiting() {
                            break;
                        }
                        thread::sleep(time::Duration::from_millis(10));
                    }
                    Err(e) => panic!("encountered IO error: {}", e),
                }
            }

            // Main thread asked for exit, cleanup all client handling threads spawned.
            for thread in threads {
                thread.join().unwrap();
            }
        })
    }

    /// Handle an MSN client's [`Command`].
    ///
    /// Returns a `(bool, Option<Box<dyn Protocol>>)` tuple where the `bool` indicate wether to
    /// disconnect or not the client as a result of the MSN [`Command`] handling and an optional new
    /// MSN [`Protocol`] to use to handle future client's commands.
    ///
    /// # Arguments:
    /// * _client_: Client MSN [`Connection`].
    /// * _dialect_: Current MSN [`Protocol`].
    /// * _command_: MSN [`Command`] to handle.
    fn handle_client_command(
        &self,
        client: &mut Connection,
        dialect: &mut Option<Box<dyn Protocol>>,
        mut command: &mut Command,
    ) -> (bool, Option<Box<dyn Protocol>>) {
        match dialect {
            // An MSN Protocol dialect was previously selected.
            Some(dialect) => {
                // Let server handle the command if it needs to.
                let handled_by_server = match command.command_type.as_str() {
                    "USR" => self.handle_usr(client, &mut command),
                    _ => None,
                };

                // If server handled the command, use the result.
                // Otherwise, let the dialect handle the command.
                match handled_by_server {
                    Some(should_disconnect) => (should_disconnect, None),
                    None => match dialect.handle(&mut command) {
                        Ok(should_disconnect) => (should_disconnect, None),
                        Err((_error, _tr_id, should_disconnect)) => (should_disconnect, None),
                    },
                }
            }

            // No MSN Protocol dialect selected.
            _ => match command.command_type.as_str() {
                "VER" => {
                    match self.handle_ver(client, &mut command) {
                        Some(selected_dialect) => match selected_dialect.as_str() {
                            // An MSN Protocol dialect was selected.
                            // Check if we support it...
                            "MSNP2" => {
                                let new_dialect = Box::new(MSNP2::new(
                                    client.clone(),
                                    self.get_database().unwrap(),
                                    self.get_identified_clients().unwrap(),
                                ));
                                (crate::protocol::KEEP_GOING, Some(new_dialect))
                            }
                            // ...well, we don't support this one.
                            _ => unimplemented!(),
                        },
                        None => {
                            // No known dialect found.
                            (crate::protocol::DISCONNECT, None)
                        }
                    }
                }
                _ => {
                    // Other command than VER sent while no dialect is selected.
                    (crate::protocol::DISCONNECT, None)
                }
            },
        }
    }

    /// Handle an MSN client [`Connection`] to this server.
    ///
    /// # Arguments:
    /// * _client_: Client MSN [`Connection`].
    fn handle_client(&self, client: &mut Connection) {
        let mut dialect: Option<Box<dyn Protocol>> = None;

        loop {
            if self.is_exiting() {
                // Server is going offline, stop handling client.
                break;
            }

            // Read client request.
            let data = match client.read() {
                Ok(Some(data)) => data,
                Ok(None) | Err(_) => {
                    // Failed to read data, stop handling client.
                    break;
                }
            };

            if data.is_empty() {
                // Client disconnected, so do we.
                break;
            }

            // Convert client request to an MSN command.
            let mut command = match Command::from_str(&data) {
                Ok(command) => command,
                Err(ParseCommandError::InvalidCommandType(_command_type, parameters)) => {
                    if parameters.len() < 1 {
                        // No parameter provided, expected at least a Transaction ID.
                        // Without TrID, we can't notify the client about which command failed.
                        // Silently ignore this command.
                    } else {
                        // Warn the client that this command was syntactically incorrect.
                        let _ = client.write(format!(
                            "{} {}",
                            ProtocolError::SyntaxError as u32,
                            parameters
                                .get(0)
                                .expect("Expected a parameter for this command.")
                        ));
                    }
                    continue;
                }
                Err(ParseCommandError::NonCRLFTerminatedCommand)
                | Err(ParseCommandError::EmptyCommand) => {
                    // Without TrID, we can't notify the client about which command failed.
                    // Silently ignore this command.
                    continue;
                }
            };

            let (disconnect_client, new_dialect) =
                self.handle_client_command(client, &mut dialect, &mut command);
            if new_dialect.is_some() {
                dialect = new_dialect;
            }

            if disconnect_client {
                client.close();
                break;
            }
        }
    }

    /// Handle MSN Protocol Versioning.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.1 Protocol Versioning][1]</cite>:
    /// >
    /// > After the client connects to a dispatch server by opening a TCP socket to port 1863, the
    /// > client and server agree on a particular protocol version before they proceed.
    ///
    /// # Arguments:
    /// * _client_: Client MSN [`Connection`].
    /// * _command_: Client MSN [`Command`].
    ///
    /// Returns `None` if client protocol versioning request is invalid or `Some(String)` otherwise:
    /// the preferred supported dialect according to user request.
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.1
    fn handle_ver(&self, client: &mut Connection, command: &mut Command) -> Option<String> {
        // C: VER TrID dialect-name{ dialect-name...}
        if command.parameters.len() < 1 {
            // No TrID provided.
            return None;
        }
        let tr_id = command.parameters.remove(0);

        let selected_dialects = self
            .get_dialects()
            .expect("No dialect set.")
            .into_iter()
            .filter(|dialect| command.parameters.contains(dialect))
            .collect::<Vec<String>>();

        if selected_dialects.is_empty() {
            // Let the client know that we don't handle any dialect she specified.
            let _ = client.write(format!("VER {} 0", tr_id));
            return None;
        }

        // Honor client dialect order of preference and use the first known to us.
        let selected_dialect = selected_dialects.first().unwrap().as_str();
        let _ = client.write(format!("VER {} {}", tr_id, selected_dialect));
        Some(selected_dialect.to_string())
    }

    /// Handle MSN Protocol Authentication.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.3 Authentication][1]
    ///
    /// # Arguments:
    /// * _client_: Client MSN [`Connection`].
    /// * _command_: Client MSN [`Command`].
    ///
    /// Returns `None` if the server did not handle the authentication or `Some(bool)` if server
    /// handled the authentication: `true` if client should be disconnected, `false` otherwise.
    ///
    /// If the server do not handle authentication, it is then handled by the currently selected
    /// MSN Protocol dialect.
    ///
    /// This can be overriden by an [`InternalServer`] to handle specific workflows.
    /// As an example, the [Dispatch Server][`DispatchServer`] refers the user to a
    /// [Notification Server][`NotificationServer`] depending of its user handle.
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.3
    fn handle_usr(&self, _client: &mut Connection, _command: &mut Command) -> Option<bool> {
        None
    }
}

#[cfg(test)]
mod tests {
    use std::net::{Ipv4Addr, SocketAddrV4};

    pub fn next_socket_v4() -> SocketAddrV4 {
        SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 0)
    }
}
