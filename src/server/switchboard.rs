//! An MSN Switchboard Server.
use crate::command::Command;
use crate::connection::Connection;
use crate::database::Database;
use crate::protocol::{MSNP2Switchboard, Protocol};
use crate::server::{ClientList, IdentifiedClientList, InternalServer};
use crate::switchboard::SessionList;

use std::collections::HashMap;
use std::net::ToSocketAddrs;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, RwLock};
use std::thread;

/// An MSN Switchboard Server.
///
/// > <cite>MSN Messenger Service 1.0 Protocol
///     > 4. MSN Messenger Server Component Overview
///     > [4.3 Switchboard Server (SS)][1]</cite>:
/// >
/// > The Switchboard Server is the component through which clients can establish lightweight
/// > communication sessions without requiring a direct network connection between clients. The
/// > common usage of the Switchboard Server is to provide instant messaging sessions.
///
/// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-4.3
pub struct SwitchboardServer {
    /// This server [Internal Switchboard Server][`InternalSwitchboardServer`] implementation.
    server: InternalSwitchboardServer,
    /// This server accept thread.
    accept_thread: thread::JoinHandle<()>,
}

impl SwitchboardServer {
    /// Start a Switchboard Server.
    ///
    /// # Arguments:
    /// * _addr_: One or more TCP socket addresses to bind to.
    /// * _dialect_: The supported dialect for this server. (e.g. "MSNP2SB")
    /// * _database_uri_: A Database URI to connect to. (e.g. "sqlite:hermsn.db")
    pub fn start<A: ToSocketAddrs>(
        addr: A,
        dialect: String,
        database_uri: String,
    ) -> SwitchboardServer {
        InternalSwitchboardServer::start(addr, dialect, database_uri)
    }

    /// Stop this Notification Server.
    pub fn stop(self) {
        self.server.stop();
        self.accept_thread.join().unwrap();
    }
}

/// An MSN Internal Switchboard Server.
#[derive(Clone)]
struct InternalSwitchboardServer {
    /// Is this server exiting?
    exiting: Arc<AtomicBool>,
    /// MSN Protocol dialects known to this server.
    dialects: Arc<Vec<String>>,
    /// Current client connections established on this server.
    all_clients: ClientList,
    /// Current client connections established on this server.
    identified_clients: IdentifiedClientList,
    /// Current sessions opened on this server.
    sessions: SessionList,
    /// Database connection pool.
    database: Arc<Database>,
}

impl InternalSwitchboardServer {
    /// Create a Switchboard Server by starting an Internal Switchboard Server.
    ///
    /// # Arguments:
    /// * _addr_: One or more TCP socket addresses to bind to.
    /// * _dialects_: A list of supported dialects for this server. (e.g. "MSNP2SB")
    /// * _database_uri_: A Database URI to connect to. (e.g. "sqlite:hermsn.db")
    fn start<A: ToSocketAddrs>(
        addr: A,
        dialect: String,
        database_uri: String,
    ) -> SwitchboardServer {
        let server = InternalSwitchboardServer {
            exiting: Arc::new(AtomicBool::new(false)),
            dialects: Arc::new(vec![dialect]),
            all_clients: Arc::new(RwLock::new(HashMap::new())),
            identified_clients: Arc::new(RwLock::new(HashMap::new())),
            database: Arc::new(Database::new(database_uri).unwrap()),
            sessions: Arc::new(RwLock::new(HashMap::new())),
        };

        // Detach accept thread from main thread
        let accept_thread =
            server.create_accept_thread(addr.to_socket_addrs().unwrap().next().unwrap());

        SwitchboardServer {
            server: server,
            accept_thread: accept_thread,
        }
    }
}

impl InternalServer for InternalSwitchboardServer {
    fn get_all_clients(&self) -> ClientList {
        Arc::clone(&self.all_clients)
    }

    fn is_exiting(&self) -> bool {
        self.exiting.load(Ordering::SeqCst)
    }

    fn stop(&self) {
        self.exiting.store(true, Ordering::SeqCst);

        // Force disconnect all currently connected clients.
        // Cf https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.10
        {
            let mut all_clients = self.all_clients.write().expect("Lock clients for write.");
            for (_, mut client) in all_clients.drain() {
                let _ = client.write("OUT SSD".to_string());
                client.close();
            }
        }
    }

    fn handle_client_command(
        &self,
        client: &mut Connection,
        dialect: &mut Option<Box<dyn Protocol>>,
        mut command: &mut Command,
    ) -> (bool, Option<Box<dyn Protocol>>) {
        match dialect {
            // An MSN Switchboard Protocol dialect was previously selected.
            Some(dialect) => match dialect.handle(&mut command) {
                Ok(should_disconnect) => (should_disconnect, None),
                Err((_error, _tr_id, should_disconnect)) => (should_disconnect, None),
            },
            // No MSN Switchboard Protocol dialect selected.
            _ => {
                // As there is no dialect selection when contacting a switch board server, lets use
                // MSNP2SB at the moment.
                let mut dialect: Box<dyn Protocol> = Box::new(MSNP2Switchboard::new(
                    client.clone(),
                    Arc::clone(&self.database),
                    Arc::clone(&self.sessions),
                ));

                match dialect.handle(&mut command) {
                    Ok(should_disconnect) => (should_disconnect, Some(dialect)),
                    Err((_error, _tr_id, should_disconnect)) => (should_disconnect, Some(dialect)),
                }
            }
        }
    }
}
