//! An MSN Dispatch Server.
use crate::command::Command;
use crate::connection::Connection;
use crate::server::ClientList;
use crate::server::InternalServer;

use std::collections::HashMap;
use std::net::ToSocketAddrs;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, RwLock};
use std::thread;

/// An MSN Dispatch Server.
///
/// > <cite>MSN Messenger Service 1.0 Protocol
///     > 4. MSN Messenger Server Component Overview
///     > [4.1 Dispatch Server (DS)][1]</cite>:
/// >
/// > The Dispatch Server is the initial point of connection between client and server. Its primary
/// > functions are protocol version negotiation, determination of which Notification Server (NS) is
/// > associated with the client making a connection (via an algorithm of the server's choosing),
/// > and referring the client to the proper NS.
///
/// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-4.1
pub struct DispatchServer {
    /// This server [Internal Dispatch Server][`InternalDispatchServer`] implementation.
    server: InternalDispatchServer,
    /// This server accept thread.
    accept_thread: thread::JoinHandle<()>,
}

impl DispatchServer {
    /// Start a Dispatch Server.
    ///
    /// # Arguments:
    /// * _addr_: One or more TCP socket addresses to bind to.
    /// * _dialects_: A list of supported dialects for this server. (e.g. "MSNP2")
    pub fn start<A: ToSocketAddrs>(addr: A, dialects: &Vec<String>) -> DispatchServer {
        InternalDispatchServer::start(addr, &dialects)
    }

    /// Stop this Dispatch Server.
    pub fn stop(self) {
        self.server.stop();
        self.accept_thread.join().unwrap();
    }
}

/// An MSN Internal Dispatch Server.
#[derive(Clone)]
struct InternalDispatchServer {
    /// Is this server exiting?
    exiting: Arc<AtomicBool>,
    /// MSN Protocol dialects known to this server.
    dialects: Arc<Vec<String>>,
    /// Current client connections established on this server.
    all_clients: ClientList,
}

impl InternalDispatchServer {
    /// Create a Dispatch Server by starting an Internal Dispatch Server.
    ///
    /// # Arguments:
    /// * _addr_: One or more TCP socket addresses to bind to.
    /// * _dialects_: A list of supported dialects for this server. (e.g. "MSNP2")
    fn start<A: ToSocketAddrs>(addr: A, dialects: &Vec<String>) -> DispatchServer {
        let server = InternalDispatchServer {
            exiting: Arc::new(AtomicBool::new(false)),
            dialects: Arc::new(dialects.clone()),
            all_clients: Arc::new(RwLock::new(HashMap::new())),
        };

        // Detach accept thread from main thread
        let accept_thread =
            server.create_accept_thread(addr.to_socket_addrs().unwrap().next().unwrap());

        DispatchServer {
            server,
            accept_thread,
        }
    }
}

impl InternalServer for InternalDispatchServer {
    fn get_dialects(&self) -> Option<Vec<String>> {
        Some(self.dialects.to_vec())
    }

    fn get_all_clients(&self) -> ClientList {
        Arc::clone(&self.all_clients)
    }

    fn is_exiting(&self) -> bool {
        self.exiting.load(Ordering::SeqCst)
    }

    fn stop(&self) {
        self.exiting.store(true, Ordering::SeqCst);

        // Force disconnect all currently connected clients.
        // Cf https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.10
        {
            let mut all_clients = self.all_clients.write().unwrap();
            for (_, mut client) in all_clients.drain() {
                let _ = client.write("OUT SSD".to_string());
                client.close();
            }
        }
    }

    fn handle_usr(&self, connection: &mut Connection, command: &mut Command) -> Option<bool> {
        // C: VER TrID dialect-name{ dialect-name...}
        if command.parameters.len() < 1 {
            // No TrID, disconnect.
            return Some(true);
        }
        let tr_id = command.parameters.remove(0);
        // S: XFR TrID ReferralType Address[:PortNo]
        let _ = connection.write(format!("XFR {} NS messenger.hotmail.com:1864", tr_id));
        Some(true)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::server::tests::next_socket_v4;

    #[test]
    fn can_stop() {
        let ds = DispatchServer::start(next_socket_v4(), &vec!["MSNP2".to_string()]);
        let s = ds.server.clone();
        assert_eq!(true, s.is_running());
        ds.stop();
        assert_eq!(false, s.is_running());
    }

    #[test]
    fn can_stop_internal() {
        let ds = InternalDispatchServer::start(next_socket_v4(), &vec!["MSNP2".to_string()]);
        let ser = ds.server.clone();
        assert_eq!(false, ser.is_exiting());
        ds.stop();
        assert_eq!(true, ser.is_exiting());
    }
}
