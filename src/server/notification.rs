//! An MSN Notification Server.
use crate::database::Database;
use crate::server::interserver::NotificationInterServer;
use crate::server::{ClientList, IdentifiedClientList, InternalServer};
use crate::user::User;

use std::collections::HashMap;
use std::net::ToSocketAddrs;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, RwLock};
use std::thread;

/// An MSN Notification Server.
///
/// > <cite>MSN Messenger Service 1.0 Protocol
///     > 4. MSN Messenger Server Component Overview
///     > [4.2 Notification Server (NS)][1]</cite>:
/// >
/// > The Notification Server is the primary server component. The client and the Notification
/// > Server authenticate, synchronize user properties, and exchange asynchronous event
/// > notifications. The client's connection to the Notification Server occurs after the referral
/// > from the Dispatch Server is completed, and persists without interruption during the user's MSN
/// > Messenger Service session.
///
/// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-4.2
pub struct NotificationServer {
    /// This server [Internal Notification Server][`InternalNotificationServer`] implementation.
    server: InternalNotificationServer,
    /// This server [Notification Inter-Server][`NotificationInterServer`] implementation.
    inter_server: NotificationInterServer,
    /// This server accept thread.
    accept_thread: thread::JoinHandle<()>,
    /// This server's inter-server accept thread.
    inter_server_accept_thread: thread::JoinHandle<()>,
}

impl NotificationServer {
    /// Start a Notification Server.
    ///
    /// # Arguments:
    /// * _addr_: One or more TCP socket addresses to bind to.
    /// * _dialects_: A list of supported dialects for this server. (e.g. "MSNP2")
    /// * _database_uri_: A Database URI to connect to. (e.g. "sqlite:hermsn.db")
    pub fn start<A: ToSocketAddrs>(
        addr: A,
        dialects: &Vec<String>,
        database_uri: String,
    ) -> NotificationServer {
        InternalNotificationServer::start(addr, &dialects, database_uri)
    }

    /// Stop this Notification Server.
    pub fn stop(self) {
        self.server.stop();
        self.inter_server.stop();
        self.accept_thread.join().unwrap();
        self.inter_server_accept_thread.join().unwrap();
    }
}

/// An MSN Internal Notification Server.
#[derive(Clone)]
struct InternalNotificationServer {
    /// Is this server exiting?
    exiting: Arc<AtomicBool>,
    /// MSN Protocol dialects known to this server.
    dialects: Arc<Vec<String>>,
    /// Current client connections established on this server.
    all_clients: ClientList,
    /// Current client connections established on this server.
    identified_clients: IdentifiedClientList,
    /// Database connection pool.
    database: Arc<Database>,
}

impl InternalNotificationServer {
    /// Create a Notification Server by starting an Internal Notification Server.
    ///
    /// # Arguments:
    /// * _addr_: One or more TCP socket addresses to bind to.
    /// * _dialects_: A list of supported dialects for this server. (e.g. "MSNP2")
    /// * _database_uri_: A Database URI to connect to. (e.g. "sqlite:hermsn.db")
    fn start<A: ToSocketAddrs>(
        addr: A,
        dialects: &Vec<String>,
        database_uri: String,
    ) -> NotificationServer {
        let database = Arc::new(Database::new(database_uri).unwrap());
        let identified_clients = Arc::new(RwLock::new(HashMap::new()));

        let server = InternalNotificationServer {
            exiting: Arc::new(AtomicBool::new(false)),
            dialects: Arc::new(dialects.clone()),
            all_clients: Arc::new(RwLock::new(HashMap::new())),
            identified_clients: Arc::clone(&identified_clients),
            database: Arc::clone(&database),
        };

        let inter_server = NotificationInterServer {
            exiting: Arc::new(AtomicBool::new(false)),
            all_clients: Arc::new(RwLock::new(HashMap::new())),
            users: identified_clients,
            database: database,
        };

        // Detach accept thread from main thread
        let accept_thread =
            server.create_accept_thread(addr.to_socket_addrs().unwrap().next().unwrap());

        let inter_server_accept_thread = inter_server
            .create_accept_thread("127.0.0.1:1865".to_socket_addrs().unwrap().next().unwrap());

        NotificationServer {
            server,
            inter_server,
            accept_thread,
            inter_server_accept_thread,
        }
    }
}

impl InternalServer for InternalNotificationServer {
    fn get_dialects(&self) -> Option<Vec<String>> {
        Some(self.dialects.to_vec())
    }

    fn get_all_clients(&self) -> ClientList {
        Arc::clone(&self.all_clients)
    }

    fn get_identified_clients(&self) -> Option<IdentifiedClientList> {
        Some(Arc::clone(&self.identified_clients))
    }

    fn get_database(&self) -> Option<Arc<Database>> {
        Some(Arc::clone(&self.database))
    }

    fn is_exiting(&self) -> bool {
        self.exiting.load(Ordering::SeqCst)
    }

    fn stop(&self) {
        self.exiting.store(true, Ordering::SeqCst);

        // Force disconnect all currently connected clients.
        // Cf https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.10
        {
            // Set all online users to offline in database.
            for (user_handle, _) in &(*self.identified_clients.read().unwrap()) {
                match User::get_from_user_handle(&user_handle, self.get_database().unwrap()) {
                    Ok(mut user) => {
                        if user.is_online() {
                            user.set_state("FLN").unwrap();
                        }
                    }
                    Err(_) => {}
                }
            }

            let mut all_clients = self.all_clients.write().unwrap();
            for (_, mut client) in all_clients.drain() {
                let _ = client.write("OUT SSD".to_string());
                client.close();
            }
        }
    }
}
