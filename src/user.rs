//! MSN users and properties.
use crate::database::Database;

use email_format::rfc5322;
use email_format::rfc5322::Parsable;
use ring::digest::{Context, SHA256};
use ring::rand;
use ring::rand::SecureRandom;

use std::collections::HashMap;
use std::fmt;
use std::sync::Arc;

/// An error that can occur while handling an MSN [`User`] data.
#[derive(Debug, Clone)]
pub enum UserError {
    DatabaseConnectError,
    DatabaseError,
    NoSuchUser,
}

impl fmt::Display for UserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            UserError::DatabaseConnectError => write!(f, "Database connect error"),
            UserError::DatabaseError => write!(f, "Database error"),
            UserError::NoSuchUser => write!(f, "No such user in database"),
        }
    }
}

/// An MSN [`User`] and its properties.
#[derive(Debug)]
pub struct User {
    pub user_handle: String,
    pub custom_user_name: String,
    pub password: String,
    pub gtc: String,
    pub blp: String,
    pub serial: u32,
    pub state: String,
    database: Arc<Database>,
}

#[cfg(feature = "sqlite")]
use r2d2_sqlite::rusqlite::named_params;

impl User {
    /// Get an MSN [`User`] and its properties from the database backend.
    ///
    /// # Arguments
    /// * _user_handle_: The MSN [`User`] user handle.
    /// * _database_: Current MSN [`Database`] backend being used.
    pub fn get_from_user_handle(
        user_handle: &str,
        database: Arc<Database>,
    ) -> Result<User, UserError> {
        let req = "SELECT user_handle, custom_user_name, password, \
                          gtc, blp, serial, state \
                   FROM users \
                   WHERE user_handle = :user_handle";

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if let Ok(mut rows) = stmt.query_named(named_params! {":user_handle": user_handle})
                {
                    if let Ok(row) = rows.next() {
                        return match row {
                            Some(row) => {
                                let custom_user_name =
                                    row.get(1).expect("No custom_user_name in result row.");
                                let password = row.get(2).expect("No password in result row.");
                                let gtc = row.get(3).expect("No gtc in result row.");
                                let blp = row.get(4).expect("No blp in result row.");
                                let serial = row.get(5).expect("No serial in result row.");
                                let state = row.get(6).expect("No state in result row.");
                                Ok(User {
                                    user_handle: user_handle.to_string(),
                                    custom_user_name,
                                    password,
                                    gtc,
                                    blp,
                                    serial,
                                    state,
                                    database,
                                })
                            }
                            None => Err(UserError::NoSuchUser),
                        };
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    /// Check that an MSN [`User`] handle is valid.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 5. Protocol Conventions
    ///     > [5.4 User Handles][1]</cite>
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-5.4
    pub fn is_user_handle_valid(user_handle: &str) -> bool {
        let mut valid = true;

        if rfc5322::types::AddrSpec::parse(user_handle.as_bytes()).is_err() {
            valid = false;
        }

        #[cfg(feature = "ietf-draft-compliance")]
        {
            if user_handle.len() > 129 {
                valid = false;
            }

            if !user_handle.ends_with("@hotmail.com") {
                valid = false;
            }
        }

        valid
    }

    /// Wether this MSN [`User`] has an online (not offline or hidden) state.
    pub fn is_online(&self) -> bool {
        match self.state.as_str() {
            "FLN" | "HDN" => false,
            _ => true,
        }
    }

    /// Check if this MSN [`User`] allow another one to message her or see her state changes.
    ///
    /// # Arguments
    /// * _other_user_handle_: The contact trying to contact this MSN [`User`].
    pub fn allows(&self, other_user_handle: &str) -> Result<bool, UserError> {
        // Check if user explicitly blocked contact
        for other_user in self.get_block_list()? {
            let handle = other_user
                .get("user_handle")
                .expect("No user_handle for this user.");
            if other_user_handle == handle {
                return Ok(false);
            }
        }

        // Check if user explicitly allowed contact
        for other_user in self.get_allow_list()? {
            let handle = other_user
                .get("user_handle")
                .expect("No user_handle for this user.");
            if other_user_handle == handle {
                return Ok(true);
            }
        }

        // User did not explicitly allowed or blocked contact. Check its privacy settings.
        Ok(self.blp == "AL")
    }

    /// Set and returns an MSN [`User`]'s cookie.
    ///
    /// # Arguments
    /// * _cookie_type_: Type of cookie to set: `CONNECT` for Switchboard identification,
    ///                  `SESSION` for Switchboard session call.
    /// * _session_id_: An `Option<&str>` specifying the Switchboard Session ID. None for a
    ///                 Switchboard identification cookie.
    pub fn add_cookie(
        &mut self,
        cookie_type: &str,
        session_id: Option<&str>,
    ) -> Result<String, UserError> {
        let req = "INSERT INTO cookies \
                        (cookie_type, user_handle, cookie, session_id, creation_time) \
                   VALUES(:cookie_type, :user_handle, :cookie, :session_id, CURRENT_TIMESTAMP)";

        let mut sha2_context = Context::new(&SHA256);
        let mut cookie_data = [0u8; 32];
        let rng = rand::SystemRandom::new();
        rng.fill(&mut cookie_data)
            .expect("Failed to fill random buffer.");
        sha2_context.update(&cookie_data);
        let cookie = hex::encode(sha2_context.finish());

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(&req) {
                if stmt
                    .execute_named(named_params! {
                        ":cookie_type": cookie_type,
                        ":user_handle": self.user_handle,
                        ":cookie": cookie,
                        ":session_id": session_id,
                    })
                    .is_ok()
                {
                    return Ok(cookie.to_string());
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Retrieve specified MSN [`User`]'s cookies.
    ///
    /// # Arguments
    /// * _cookie_type_: Type of cookie to retrieve: `CONNECT` for Switchboard identification,
    ///                  `SESSION` for Switchboard session answering call.
    pub fn get_cookies(
        &mut self,
        cookie_type: &str,
    ) -> Result<Vec<HashMap<&str, String>>, UserError> {
        let req = "SELECT cookie, session_id \
                   FROM cookies \
                   WHERE user_handle = :user_handle AND cookie_type = :cookie_type";
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(&req) {
                if let Ok(mut rows) = stmt.query_named(
                    named_params! {":user_handle": self.user_handle, ":cookie_type": cookie_type},
                ) {
                    let mut results = Vec::new();
                    while let Ok(row) = rows.next() {
                        match row {
                            Some(row) => {
                                let mut result = HashMap::new();
                                result.insert(
                                    "cookie",
                                    row.get(0).expect("No cookie in result row."),
                                );
                                let session_id: u32 = row.get(1).unwrap_or(0);
                                result.insert("session_id", session_id.to_string());
                                results.push(result);
                            }
                            None => {
                                return Ok(results);
                            }
                        }
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    /// Delete specified MSN [`User`]'s cookie.
    pub fn delete_cookie(&mut self, cookie: &str) -> Result<(), UserError> {
        let req = "DELETE FROM cookies \
                   WHERE user_handle = :user_handle AND cookie = :cookie";

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(&req) {
                if stmt
                    .execute_named(named_params! {
                    ":user_handle": self.user_handle,
                    ":cookie": cookie,
                    })
                    .is_ok()
                {
                    return Ok(());
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Set MSN [`User`] serial.
    ///
    /// # Arguments
    /// * _serial_: The MSN [`User`] new serial.
    pub fn increment_serial(&mut self) -> Result<(), UserError> {
        let old_serial = self.serial;
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(mut conn) = self.database.get() {
            if let Ok(tx) = conn.transaction() {
                let req = "UPDATE users SET serial = serial + 1 WHERE user_handle = :user_handle";
                if let Ok(mut stmt) = tx.prepare(req) {
                    if stmt
                        .execute_named(named_params! {":user_handle": self.user_handle})
                        .is_ok()
                    {
                        let req = "SELECT serial FROM users WHERE user_handle = :user_handle";
                        if let Ok(mut stmt) = tx.prepare(req) {
                            if let Ok(mut rows) =
                                stmt.query_named(named_params! {":user_handle": self.user_handle})
                            {
                                if let Ok(row) = rows.next() {
                                    if let Some(row) = row {
                                        self.serial = row.get(0).expect("No serial in result row.");
                                    }
                                }
                            }
                        }
                    }
                }
                match tx.commit() {
                    Ok(()) => {
                        // Transaction went fine.
                        return Ok(());
                    }
                    Err(_) => {
                        // Revert to old serial if transaction failed.
                        self.serial = old_serial;
                    }
                };
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    /// Set MSN [`User`] state.
    ///
    /// # Arguments
    /// * _state_: The MSN [`User`] new state.
    pub fn set_state(&mut self, state: &str) -> Result<(), UserError> {
        let req = "UPDATE users SET state = :state WHERE user_handle = :user_handle";
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if stmt
                    .execute_named(
                        named_params! {":user_handle": self.user_handle, ":state": state},
                    )
                    .is_ok()
                {
                    self.state = state.to_string();
                    return Ok(());
                }
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    /// Set MSN [`User`] reverse list prompting.
    ///
    /// # Arguments
    /// * _gtc_: The MSN [`User`] new reverse list prompting value.
    pub fn set_gtc(&mut self, gtc: &str) -> Result<(), UserError> {
        let req = "UPDATE users SET gtc = :gtc WHERE user_handle = :user_handle";

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if stmt
                    .execute_named(named_params! {
                        ":user_handle": self.user_handle,
                        ":gtc": gtc
                    })
                    .is_ok()
                {
                    return match self.increment_serial() {
                        Ok(()) => {
                            self.gtc = gtc.to_string();
                            Ok(())
                        }
                        Err(error) => Err(error),
                    };
                }
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    /// Set MSN [`User`] privacy mode.
    ///
    /// # Arguments
    /// * _blp_: The MSN [`User`] new privacy mode value.
    pub fn set_blp(&mut self, blp: &str) -> Result<(), UserError> {
        let req = "UPDATE users SET blp = :blp WHERE user_handle = :user_handle";

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if stmt
                    .execute_named(named_params! {
                        ":user_handle": self.user_handle,
                        ":blp": blp
                    })
                    .is_ok()
                {
                    return match self.increment_serial() {
                        Ok(()) => {
                            self.blp = blp.to_string();
                            Ok(())
                        }
                        Err(error) => Err(error),
                    };
                }
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    /// Get the MSN [`User`] Forward List.
    pub fn get_forward_list(&self) -> Result<Vec<HashMap<&str, String>>, UserError> {
        let req = "SELECT fl.contact_user_handle, fl.custom_user_name, u.state \
                   FROM forward_list AS fl \
                   INNER JOIN users AS u ON fl.contact_user_handle = u.user_handle \
                   WHERE fl.user_handle = :user_handle";
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if let Ok(mut rows) =
                    stmt.query_named(named_params! {":user_handle": self.user_handle})
                {
                    let mut results = Vec::new();
                    while let Ok(row) = rows.next() {
                        match row {
                            Some(row) => {
                                let mut result = HashMap::new();
                                result.insert(
                                    "user_handle",
                                    row.get(0).expect("No user_handle in result row."),
                                );
                                result.insert(
                                    "custom_user_name",
                                    row.get(1).expect("No custom_user_name in result row."),
                                );
                                result
                                    .insert("state", row.get(2).expect("No state in result row."));
                                results.push(result);
                            }
                            None => {
                                return Ok(results);
                            }
                        }
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Get the MSN [`User`] Allow List.
    pub fn get_allow_list(&self) -> Result<Vec<HashMap<&str, String>>, UserError> {
        let req = "SELECT contact_user_handle, custom_user_name \
                   FROM allow_list \
                   WHERE user_handle = :user_handle";
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if let Ok(mut rows) =
                    stmt.query_named(named_params! {":user_handle": self.user_handle})
                {
                    let mut results = Vec::new();
                    while let Ok(row) = rows.next() {
                        match row {
                            Some(row) => {
                                let mut result = HashMap::new();
                                result.insert(
                                    "user_handle",
                                    row.get(0).expect("No user_handle in result row."),
                                );
                                result.insert(
                                    "custom_user_name",
                                    row.get(1).expect("No custom_user_name in result row."),
                                );
                                results.push(result);
                            }
                            None => {
                                return Ok(results);
                            }
                        }
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Get the MSN [`User`] Block List.
    pub fn get_block_list(&self) -> Result<Vec<HashMap<&str, String>>, UserError> {
        let req = "SELECT contact_user_handle, custom_user_name \
                   FROM block_list \
                   WHERE user_handle = :user_handle";
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if let Ok(mut rows) =
                    stmt.query_named(named_params! {":user_handle": self.user_handle})
                {
                    let mut results = Vec::new();
                    while let Ok(row) = rows.next() {
                        match row {
                            Some(row) => {
                                let mut result = HashMap::new();
                                result.insert(
                                    "user_handle",
                                    row.get(0).expect("No user_handle in result row."),
                                );
                                result.insert(
                                    "custom_user_name",
                                    row.get(1).expect("No custom_user_name in result row."),
                                );
                                results.push(result);
                            }
                            None => {
                                return Ok(results);
                            }
                        }
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Get the MSN [`User`] Reverse List.
    pub fn get_reverse_list(&self) -> Result<Vec<HashMap<&str, String>>, UserError> {
        let req = "SELECT fl.user_handle, u.state \
                   FROM forward_list AS fl \
                   INNER JOIN users AS u ON fl.user_handle = u.user_handle \
                   WHERE fl.contact_user_handle = :contact_user_handle";
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if let Ok(mut rows) =
                    stmt.query_named(named_params! {":contact_user_handle": self.user_handle})
                {
                    let mut results = Vec::new();
                    while let Ok(row) = rows.next() {
                        match row {
                            Some(row) => {
                                let mut result = HashMap::new();
                                result.insert(
                                    "user_handle",
                                    row.get(0).expect("No user_handle in result row."),
                                );
                                result
                                    .insert("state", row.get(1).expect("No state in result row."));
                                results.push(result);
                            }
                            None => {
                                return Ok(results);
                            }
                        }
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Add an MSN User handle to one of this MSN [`User`] Lists.
    ///
    /// # Arguments
    /// * _list_: The list to add the user to.
    /// * _user_handle_: The User's handle.
    /// * _custom_user_name_: The User's custom name.
    pub fn add_to_list(
        &mut self,
        list: &str,
        user_handle: &str,
        custom_user_name: &str,
    ) -> Result<(), UserError> {
        let list = match list {
            "FL" => "forward_list",
            "AL" => "allow_list",
            "BL" => "block_list",
            _ => panic!("Tried to add user to an unknown list: {}.", list),
        };

        let req = format!(
            "INSERT INTO {} (user_handle, contact_user_handle, custom_user_name) \
             VALUES(:user_handle, :contact_user_handle, :custom_user_name)",
            list
        );
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(&req) {
                if stmt
                    .execute_named(named_params! {
                        ":user_handle": self.user_handle,
                        ":contact_user_handle": user_handle,
                        ":custom_user_name": custom_user_name
                    })
                    .is_ok()
                {
                    return self.increment_serial();
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Remove an MSN User handle from one of this MSN [`User`] Lists.
    ///
    /// # Arguments
    /// * _list_: The list to remove the user from.
    /// * _user_handle_: The User's handle.
    pub fn remove_from_list(&mut self, list: &str, user_handle: &str) -> Result<(), UserError> {
        let list = match list {
            "FL" => "forward_list",
            "AL" => "allow_list",
            "BL" => "block_list",
            _ => panic!("Tried to remove a user from an unknown list: {}.", list),
        };

        let req = format!(
            "DELETE FROM {} WHERE contact_user_handle = :contact_user_handle",
            list
        );
        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(&req) {
                if stmt
                    .execute_named(named_params! {":contact_user_handle": user_handle})
                    .is_ok()
                {
                    return self.increment_serial();
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }

    /// Change this MSN [`User`] custom user name or one of it's contacts.
    ///
    /// # Arguments
    /// * _user_handle_: The MSN [`User`] to rename.
    /// * _custom_user_name_: The new custom user name of the user.
    pub fn rename(&mut self, user_handle: &str, custom_user_name: &str) -> Result<(), UserError> {
        let req = if user_handle == self.user_handle {
            "UPDATE users \
             SET custom_user_name = :custom_user_name \
             WHERE user_handle = :user_handle"
        } else {
            "UPDATE forward_list \
             SET custom_user_name = :custom_user_name \
             WHERE contact_user_handle = :user_handle"
        };

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = self.database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if stmt
                    .execute_named(named_params! {
                        ":user_handle": user_handle,
                        ":custom_user_name": custom_user_name
                    })
                    .is_ok()
                {
                    return match self.increment_serial() {
                        Ok(()) => {
                            if self.user_handle == user_handle {
                                self.custom_user_name = custom_user_name.to_string()
                            }
                            Ok(())
                        }
                        Err(error) => Err(error),
                    };
                }
            }
            error_type = UserError::DatabaseError;
        }
        Err(error_type)
    }

    pub fn search<'a>(
        database: Arc<Database>,
        first_name: &'a str,
        last_name: &'a str,
        city: &'a str,
        state: &'a str,
        country: &'a str,
    ) -> Result<Vec<HashMap<&'a str, String>>, UserError> {
        let req = "SELECT user_handle, first_name, last_name, city, state, country \
                   FROM directory \
                   WHERE first_name LIKE :first_name \
                         AND last_name LIKE :last_name \
                         AND (city IS NULL OR city LIKE :city) \
                         AND (state IS NULL OR state LIKE :state) \
                         AND (country IS NULL OR country LIKE :country)";

        let country = if country == "*" { "%" } else { country };
        let city = if city == "*" { "%" } else { city };
        let state = if state == "*" { "%" } else { state };

        let mut error_type = UserError::DatabaseConnectError;
        if let Ok(conn) = database.get() {
            if let Ok(mut stmt) = conn.prepare(req) {
                if let Ok(mut rows) = stmt.query_named(
                    named_params! {":first_name": first_name, ":last_name": last_name,
                    ":city": city, ":state": state, ":country": country},
                ) {
                    let mut results = Vec::new();
                    while let Ok(row) = rows.next() {
                        match row {
                            Some(row) => {
                                let mut result = HashMap::new();
                                result.insert(
                                    "user_handle",
                                    row.get(0).expect("No user_handle in result row."),
                                );
                                result.insert(
                                    "first_name",
                                    row.get(1).expect("No first_name in result row."),
                                );
                                result.insert(
                                    "last_name",
                                    row.get(2).expect("No last_name in result row."),
                                );
                                if let Ok(city) = row.get(3) {
                                    result.insert("city", city);
                                }
                                if let Ok(state) = row.get(4) {
                                    result.insert("state", state);
                                }
                                if let Ok(country) = row.get(5) {
                                    result.insert("country", country);
                                }
                                results.push(result);
                            }
                            None => {
                                return Ok(results);
                            }
                        }
                    }
                }
            }
            error_type = UserError::DatabaseError;
        }

        Err(error_type)
    }
}
