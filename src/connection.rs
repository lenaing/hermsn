//! MSN [`Connection`] representation.
use std::io::*;
use std::net::TcpStream;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

/// An error that can occur while transmitting data through an MSN [`Connection`].
pub enum ConnectionError {
    ReadError,
    WriteError,
}

/// An MSN [`Connection`].
///
/// > <cite>MSN Messenger Service 1.0 Protocol
///     > 5. Protocol Conventions
///     > [5.1 Connection Type][1]</cite>:
/// >
/// > The MSN Messenger Protocol currently works over TCP/IP.
/// > The MSN Messenger server components support connections over port numbers 1863.
///
/// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-5.1
pub struct Connection {
    /// The underlying associated TCP Stream.
    stream: TcpStream,
    /// Is this connection and its associated TCP Stream closed?
    closed: Arc<AtomicBool>,
}

impl Clone for Connection {
    fn clone(&self) -> Self {
        Self {
            stream: self.stream.try_clone().expect("Failed to clone TcpStream."),
            closed: Arc::clone(&self.closed),
        }
    }
}

impl Connection {
    /// Create a new MSN [`Connection`] from a TCP Stream.
    pub fn new(stream: &TcpStream) -> Self {
        Self {
            stream: stream.try_clone().expect("Failed to clone TcpStream."),
            closed: Arc::new(AtomicBool::new(false)),
        }
    }

    /// Read data from the MSN [`Connection`].
    pub fn read(&mut self) -> std::result::Result<Option<String>, ConnectionError> {
        if self.closed.load(Ordering::SeqCst) {
            return Ok(None);
        }
        let mut data = String::new();
        // Inefficient, but we can't use a BufReader here.
        let mut buf = [0; 1];
        loop {
            if self.stream.read_exact(&mut buf).is_err() {
                return Err(ConnectionError::ReadError);
            }
            data.push(buf[0] as char);
            if buf[0] == b'\n' {
                break;
            }
        }
        println!("<< {}", data.trim_end_matches(|c| c == '\r' || c == '\n'));
        Ok(Some(data))
    }

    /// Read data payload from the MSN [`Connection`].
    pub fn read_payload(
        &mut self,
        length: u64,
    ) -> std::result::Result<Option<Vec<u8>>, ConnectionError> {
        if self.closed.load(Ordering::SeqCst) {
            return Ok(None);
        }
        let mut data = Vec::with_capacity(length as usize);
        let mut handle = self
            .stream
            .try_clone()
            .expect("Failed to clone TcpStream.")
            .take(length);
        if handle.read_to_end(&mut data).is_ok() {
            let debug_data = match std::str::from_utf8(&data) {
                Ok(data) => data.to_string(),
                Err(_) => format!("{:02x?}", data),
            };
            let banner = "-".repeat(80);
            println!("{}\n{}\n{}", banner, debug_data, banner);

            Ok(Some(data))
        } else {
            Err(ConnectionError::ReadError)
        }
    }

    /// Write data to the MSN [`Connection`].
    pub fn write(&mut self, data: String) -> std::result::Result<(), ConnectionError> {
        if self.closed.load(Ordering::SeqCst) {
            return Ok(());
        }
        println!(">> {}", data);
        if self
            .stream
            .write(format!("{}\r\n", data).as_bytes())
            .is_err()
        {
            return Err(ConnectionError::WriteError);
        }
        if self.stream.flush().is_err() {
            return Err(ConnectionError::WriteError);
        }
        Ok(())
    }

    /// Write data payload to the MSN [`Connection`].
    pub fn write_payload(&mut self, data: &Vec<u8>) -> std::result::Result<(), ConnectionError> {
        if self.closed.load(Ordering::SeqCst) {
            return Ok(());
        }

        let debug_data = match std::str::from_utf8(&data) {
            Ok(data) => data.to_string(),
            Err(_) => format!("{:02x?}", data),
        };
        let banner = "-".repeat(80);
        println!("{}\n{}\n{}", banner, debug_data, banner);

        if self.stream.write(data).is_err() {
            return Err(ConnectionError::WriteError);
        }
        if self.stream.flush().is_err() {
            return Err(ConnectionError::WriteError);
        }
        Ok(())
    }

    /// Close the MSN [`Connection`].
    ///
    /// Mark the connection as closed and shutdown both ends of the underlying TCP stream.
    pub fn close(&mut self) {
        if self.closed.load(Ordering::SeqCst) {
            return;
        }
        self.closed.store(true, Ordering::SeqCst);

        match self.stream.shutdown(std::net::Shutdown::Both) {
            Ok(()) => {}
            Err(error) => match error.kind() {
                ErrorKind::NotConnected => {
                    // Connection already closed from client side.
                }
                other_error => {
                    panic!("Failed to close connection stream: {:?}", other_error);
                }
            },
        }
    }
}
