//! # hermsn
//!
//! This library provide tools to run an [MSN Messenger][1] service.
//!
//! ## Supported MSN Protocols
//!
//! * MSNP2 : Based on the [original 1999 Internet Draft][2].
//!
//! ## Quickstart
//!
//! ### Server components
//!
//! In a console, launch the [Dispatch Server][`DispatchServer`]:
//! ```console
//! cargo run --example hermsn-ds
//! ```
//!
//! In another console, launch the [Notification Server][`NotificationServer`]:
//! ```console
//! cargo run --example hermsn-ns
//! ```
//!
//! ### Client
//!
//! Install the original MSN Messenger Service and modify your hosts file to make
//! `messenger.hotmail.com` point to your server IP.
//!
//! Connect. Have fun.
//!
//! [1]: https://en.wikipedia.org/wiki/Windows_Live_Messenger#MSN_Messenger_1.0%E2%80%937.5_(1999%E2%80%932005)
//! [2]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00
#![warn(missing_docs)]

mod server;

pub use server::DispatchServer;
pub use server::NotificationServer;
pub use server::SwitchboardServer;
mod command;
mod connection;
mod database;
mod hash;
mod protocol;
mod switchboard;
mod user;
