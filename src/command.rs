//! MSN [`Command`] implementation.
use std::fmt;
use std::str::FromStr;

/// An error that can occur while parsing an MSN [`Command`] from a string.
#[derive(Debug, Clone)]
pub enum ParseCommandError {
    NonCRLFTerminatedCommand,
    EmptyCommand,
    InvalidCommandType(String, Vec<String>),
}

impl fmt::Display for ParseCommandError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            ParseCommandError::EmptyCommand => write!(f, "Empty command"),
            ParseCommandError::NonCRLFTerminatedCommand => write!(f, "Non CRLF-terminated command"),
            ParseCommandError::InvalidCommandType(command_type, parameters) => {
                write!(
                    f,
                    "Invalid Command Type: {}, Parameters: [{}]",
                    command_type,
                    parameters.join(", ")
                )
            }
        }
    }
}

/// An MSN [`Command`].
///
/// > <cite>MSN Messenger Service 1.0 Protocol
///     > 5. Protocol Conventions
///     > [5.2 Command Syntax][1]</cite>:
/// >
/// > MSN Messenger Protocol command syntax is ASCII and single line-based.
/// > Commands begin with a case-sensitive, three-letter command type, followed by zero or more
/// > parameters, and terminated by CRLF.
/// > Parameters are separated by one or more whitespace characters and cannot contain whitespace
/// > characters. Parameters that contain spaces or extended (non 7-bit ASCII) characters should be
/// > encoded using URL-style encoding (e.g. "%20" for space).
/// > Some commands accept unencoded binary data. In these cases, the length of the data is
/// > transmitted as part of the command, and the data is transmitted immediately following a CRLF
/// > of the command.
///
/// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-5.2
#[derive(Debug)]
pub struct Command {
    /// Case-sensitive, three-letter command type.
    pub command_type: String,
    /// URL-encoded command parameters.
    pub parameters: Vec<String>,
}

impl FromStr for Command {
    type Err = ParseCommandError;

    fn from_str(command: &str) -> Result<Self, Self::Err> {
        if !command.ends_with("\r\n") {
            return Err(ParseCommandError::NonCRLFTerminatedCommand);
        }

        let mut parameters = command
            .trim_end_matches(|c| c == '\r' || c == '\n')
            .split_ascii_whitespace()
            .map(String::from)
            .collect::<Vec<String>>();

        if parameters.len() < 1 {
            return Err(ParseCommandError::EmptyCommand);
        }

        let command_type = parameters.remove(0);
        if command_type.is_empty()
            || command_type.len() != 3
            || (!command_type.chars().all(|c| char::is_ascii_alphabetic(&c))
                && !command_type.chars().all(|c| char::is_digit(c, 10)))
        {
            return Err(ParseCommandError::InvalidCommandType(
                command_type,
                parameters,
            ));
        }

        Ok(Command {
            command_type: command_type,
            parameters: parameters,
        })
    }
}
