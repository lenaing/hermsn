use crate::connection::Connection;
use crate::database::Database;
use crate::server::IdentifiedClientList;
use crate::user::{User, UserError};

use rand::Rng;

use std::collections::HashMap;
use std::sync::{Arc, RwLock};

pub type SessionList = Arc<RwLock<HashMap<String, Session>>>;

/// An MSN Switchboard Session.
#[derive(Clone)]
pub struct Session {
    pub id: u32,
    /// Users currently in the session.
    users: IdentifiedClientList,
    /// Database connection pool.
    database: Arc<Database>,
}

impl Session {
    /// Creates an MSN Switchboard Session.
    ///
    /// # Arguments
    /// * _database_: Current MSN [`Database`] backend being used.
    pub fn new(database: Arc<Database>) -> Session {
        let mut rng = rand::thread_rng();
        Session {
            id: rng.gen::<u32>(),
            users: Arc::new(RwLock::new(HashMap::new())),
            database,
        }
    }

    /// Add a user to a Switchboard Session.
    ///
    /// # Arguments
    /// * _user_: The MSN [`User`] joining the session.
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _client_: This user's MSN [`Connection`].
    pub fn add_user(
        &self,
        user: &User,
        tr_id: &str,
        mut client: Connection,
    ) -> Result<(), UserError> {
        // Only for joining users...
        if tr_id != "-1" {
            let users = self.users.read().expect("Lock users for read.");
            let total = users.len();

            for (index, (other_user_handle, other_client)) in users.iter().enumerate() {
                match User::get_from_user_handle(other_user_handle, Arc::clone(&self.database)) {
                    Ok(other_user) => {
                        // Inform this user that another participant is already in the session.
                        let _ = client.write(format!(
                            "IRO {} {} {} {} {}",
                            tr_id,
                            index + 1,
                            total,
                            other_user.user_handle,
                            other_user.custom_user_name
                        ));

                        // Inform other participants that this user joined the session.
                        let _ = other_client.clone().write(format!(
                            "JOI {} {}",
                            user.user_handle, user.custom_user_name
                        ));
                    }
                    Err(error) => {
                        // Failed to retrieve other user informations.
                        return Err(error);
                    }
                }
            }

            let _ = client.write(format!("ANS {} OK", tr_id));
        }

        // Append user connection to active users.
        {
            self.users
                .write()
                .expect("Lock users for write.")
                .insert(user.user_handle.to_string(), client);
        }
        Ok(())
    }

    /// Remove a user from a Switchboard Session.
    ///
    /// Returns `true` if session do not have anymore participant, `false` otherwise.
    ///
    /// # Arguments
    /// * _user_: The MSN [`User`] leaving the session.
    pub fn remove_user(&self, user: &User) -> bool {
        // Notify other participants that this user quit the session.
        {
            for (user_handle, client) in self.users.read().expect("Lock users for read.").iter() {
                if user_handle.as_str() != user.user_handle {
                    let _ = client.clone().write(format!("BYE {}", user.user_handle));
                }
            }
        }

        // Remove user connection to active users.
        {
            self.users
                .write()
                .expect("Lock users for write.")
                .remove(&user.user_handle);
        }

        {
            self.users.read().expect("Lock users for read.").is_empty()
        }
    }

    /// Send a message to a Switchboard Session.
    ///
    /// Returns `true` if all session participant received the message or `false` otherwise.
    ///
    /// # Arguments
    /// * _user_: The MSN [`User`] sending the message.
    /// * _payload_: The message.
    pub fn send_message(&self, user: &User, payload: Vec<u8>) -> bool {
        let mut success = true;
        let users = self.users.read().expect("Lock users for read.");
        for (other_user_handle, connection) in users.iter() {
            if other_user_handle.as_str() != user.user_handle {
                let mut connection = connection.clone();

                if connection
                    .write(format!(
                        "MSG {} {} {}",
                        user.user_handle,
                        user.custom_user_name,
                        payload.len()
                    ))
                    .is_err()
                {
                    success = false;
                }

                if connection.write_payload(&payload).is_err() {
                    success = false;
                }
            }
        }
        success
    }
}
