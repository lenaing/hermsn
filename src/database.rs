//! MSN [`Database`] backend implementation.
use std::fmt;

/// A database error that can occur while querying an MSN [`Database`] backend.
#[derive(Debug, Clone)]
pub enum DatabaseError {
    UnsupportedDatabaseDriver(String),
    FailedToGetConnection(String),
    FailedToCreateDatabasePool,
}

impl fmt::Display for DatabaseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            DatabaseError::UnsupportedDatabaseDriver(driver) => {
                write!(f, "Unsupported database driver: {}", driver)
            }
            DatabaseError::FailedToGetConnection(reason) => {
                write!(f, "Failed to get database connection: {}", reason)
            }
            DatabaseError::FailedToCreateDatabasePool => {
                write!(f, "Failed to create database pool")
            }
        }
    }
}

/// A database connection pool.
#[cfg(feature = "sqlite")]
pub type DatabasePool = r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>;

/// A database connection.
#[cfg(feature = "sqlite")]
pub type DatabaseConnection = r2d2::PooledConnection<r2d2_sqlite::SqliteConnectionManager>;

/// An MSN [`Database`] backend.
#[derive(Debug)]
pub struct Database {
    pool: DatabasePool,
}

impl Database {
    pub fn new(uri: String) -> Result<Database, DatabaseError> {
        let uri_components: Vec<&str> = uri.splitn(2, ":").collect();
        let driver = uri_components.get(0).expect("No driver specified in URI.");

        match driver.as_ref() {
            "sqlite" => {
                if !cfg!(feature = "sqlite") {
                    return Err(DatabaseError::UnsupportedDatabaseDriver(driver.to_string()));
                }
            }
            _ => return Err(DatabaseError::UnsupportedDatabaseDriver(driver.to_string())),
        };

        #[cfg(feature = "sqlite")]
        let pool = match r2d2::Pool::new(r2d2_sqlite::SqliteConnectionManager::file(
            uri_components
                .get(1)
                .expect("No Sqlite Database file specified in URI."),
        )) {
            Ok(pool) => pool,
            Err(_) => return Err(DatabaseError::FailedToCreateDatabasePool),
        };

        Ok(Database { pool: pool })
    }

    pub fn get(&self) -> Result<DatabaseConnection, DatabaseError> {
        match self.pool.get() {
            Ok(conn) => Ok(conn),
            Err(error) => Err(DatabaseError::FailedToGetConnection(error.to_string())),
        }
    }
}
