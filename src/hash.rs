//! MSN Protocol hash utilities.

/// An error that can occur while checking MSNP MD5 password components.
#[derive(Debug, Clone)]
pub enum MSNPHashError {
    AlgorithmNotFound,
    SaltNotFound,
    HashNotFound,
    WrongAlgorithm,
}

/// Extract salt from an MSNP MD5 password.
///
/// # Arguments:
/// * _password_: A password string. (e.g. `$msnp$sjAoq4ny$328d161eec2b75f97784444b4b947c38`)
///
/// Returns the salt found (e.g. `sjAoq4ny`) or an [`MSNPHashError`].
pub fn extract_salt_from_md5_password(password: &str) -> Result<String, MSNPHashError> {
    let password_components: Vec<String> = password.splitn(4, "$").map(|s| s.to_string()).collect();
    match password_components.get(1) {
        Some(algorithm) => {
            if algorithm == "msnp" {
                match password_components.get(2) {
                    Some(salt) => Ok(salt.to_string()),
                    None => Err(MSNPHashError::SaltNotFound),
                }
            } else {
                Err(MSNPHashError::WrongAlgorithm)
            }
        }
        None => Err(MSNPHashError::AlgorithmNotFound),
    }
}

/// Extract computed hash from an MSNP MD5 password.
///
/// # Arguments:
/// * _password_: A password string. (e.g. `$msnp$sjAoq4ny$328d161eec2b75f97784444b4b947c38`)
///
/// Returns the hash found (e.g. `328d161eec2b75f97784444b4b947c38`) or an [`MSNPHashError`].
pub fn extract_hash_from_md5_password(password: &str) -> Result<String, MSNPHashError> {
    let password_components: Vec<String> = password.splitn(4, "$").map(|s| s.to_string()).collect();
    match password_components.get(1) {
        Some(algorithm) => {
            if algorithm == "msnp" {
                match password_components.get(3) {
                    Some(hash) => Ok(hash.to_string()),
                    None => Err(MSNPHashError::HashNotFound),
                }
            } else {
                Err(MSNPHashError::WrongAlgorithm)
            }
        }
        None => Err(MSNPHashError::AlgorithmNotFound),
    }
}
