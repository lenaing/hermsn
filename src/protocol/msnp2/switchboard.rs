//! [`MSNP2SB`] MSN Switchboard Protocol handling.
use crate::command::Command;
use crate::connection::Connection;
use crate::database::Database;
use crate::protocol::{HandleResult, Protocol, ProtocolError, DISCONNECT, KEEP_GOING};
use crate::switchboard::{Session, SessionList};
use crate::user::{User, UserError};

use std::net::TcpStream;
use std::str::FromStr;
use std::sync::Arc;

/// An MSNP2 Switchboard protocol handler.
pub struct MSNP2Switchboard {
    /// Current client MSN [`Connection`] being handled.
    client: Connection,
    /// Current MSN [`Database`] backend being used.
    database: Arc<Database>,
    /// Client's associated MSN Switchboard [`Session`].
    session: Option<Session>,
    /// Current MSN Switchboard [`Session`]s active on this Switchboard Server.
    sessions: SessionList,
    /// Client's associated MSN [`User`].
    user: Option<User>,
}

impl MSNP2Switchboard {
    /// Creates an MSNP2 Switchboard protocol handler.
    ///
    /// # Arguments
    /// * _client_: An MSN [`Connection`] to handle.
    /// * _database_: Current MSN [`Database`] backend being used.
    /// * _sessions_: Current MSN Switchboard [`Session`]s active on this Switchboard Server.
    pub fn new(
        client: Connection,
        database: Arc<Database>,
        sessions: SessionList,
    ) -> MSNP2Switchboard {
        MSNP2Switchboard {
            client,
            database,
            session: None,
            sessions,
            user: None,
        }
    }

    /// Handle Switchboard Connection.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 8. Session based Instant Messaging Protocol Details
    ///     > [8.2 Switchboard Connections and Authentication][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-8.2
    fn handle_usr(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: USR TrID UserHandle AuthResponseInfo
        // S: USR TrID OK UserHandle FriendlyName

        let user_handle = command.parameters.get(0);
        let auth_response_info = command.parameters.get(1);

        if user_handle.is_none() || auth_response_info.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }

        let user_handle = user_handle.expect("No user handle in command.");
        let auth_response_info = auth_response_info.expect("No auth response info in command.");

        match User::get_from_user_handle(&user_handle, Arc::clone(&self.database)) {
            Ok(mut user) => {
                let mut found_cookie = false;
                if let Ok(cookies) = user.get_cookies("CONNECT") {
                    for cookie in cookies {
                        let cookie_value = cookie
                            .get("cookie")
                            .expect("No cookie value for this cookie.");
                        if cookie_value == auth_response_info {
                            found_cookie = true;
                        }
                    }
                }

                if !found_cookie {
                    return self.handle_err(ProtocolError::AuthenticationFailed, tr_id, DISCONNECT);
                }

                if user.delete_cookie(auth_response_info).is_err() {
                    // Failed to delete session cookie.
                }

                if self
                    .client
                    .write(format!(
                        "USR {} OK {} {}",
                        tr_id, user.user_handle, user.custom_user_name
                    ))
                    .is_err()
                {
                    return Ok(DISCONNECT);
                }
                self.user = Some(user);
                Ok(KEEP_GOING)
            }
            Err(UserError::NoSuchUser) => {
                // Fail as internal error to avoid user guessing.
                self.handle_err(ProtocolError::InternalError, tr_id, DISCONNECT)
            }
            Err(UserError::DatabaseConnectError) => {
                self.handle_err(ProtocolError::DatabaseConnectError, tr_id, DISCONNECT)
            }
            Err(UserError::DatabaseError) => {
                self.handle_err(ProtocolError::DatabaseError, tr_id, DISCONNECT)
            }
        }
    }

    /// Handle Switchboard Session Calls.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 8. Session based Instant Messaging Protocol Details
    ///     > [8.3 Inviting Users to a Switchboard Session][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-8.3
    fn handle_cal(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: CAL TrID UserHandle
        // S: CAL TrID Status SessionID

        let other_user_handle = command.parameters.get(0);

        if other_user_handle.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }

        let other_user_handle = other_user_handle.expect("No user handle in command.");

        if !User::is_user_handle_valid(&other_user_handle) {
            return self.handle_err(ProtocolError::InvalidUsername, tr_id, DISCONNECT);
        }

        // The Switchboard Server do not have a connection to notify the a contact that it is
        // called, but the Notification Server does. So let the NS call the contact for us.
        let mut interserver = match TcpStream::connect("127.0.0.1:1865") {
            Ok(stream) => Connection::new(&stream),
            Err(_) => {
                return self.handle_err(ProtocolError::PeerNSIsDown, tr_id, DISCONNECT);
            }
        };

        let session = Session::new(Arc::clone(&self.database));
        let user = self.user.as_ref().expect("User not connected.");

        if interserver
            .write(format!(
                "INT MSNP2 CAL {} {} {} {}",
                user.user_handle, user.custom_user_name, other_user_handle, session.id
            ))
            .is_err()
        {
            return self.handle_err(ProtocolError::PeerNSIsDown, tr_id, DISCONNECT);
        }

        match interserver.read() {
            Ok(Some(command)) => {
                if command.is_empty() {
                    // Client disconnected, so do we.
                    return Ok(DISCONNECT);
                }

                match Command::from_str(&command) {
                    Ok(interserver_answer) => {
                        if "CAL" == interserver_answer.command_type.as_str() {
                            let call_result = interserver_answer
                                .parameters
                                .get(0)
                                .expect("No CAL result in Inter-Server answer.");
                            if call_result == "RINGING" {
                                if session.add_user(user, "-1", self.client.clone()).is_ok() {
                                    self.sessions
                                        .write()
                                        .expect("Lock sessions for write.")
                                        .insert(session.id.to_string(), session.clone());

                                    let _ = self
                                        .client
                                        .write(format!("CAL {} RINGING {}", tr_id, session.id));
                                    self.session = Some(session);

                                    return Ok(KEEP_GOING);
                                }
                            }
                        }
                    }
                    Err(_) => {
                        // Failed to parse command.
                        // Answer to the client with an Internal Error from us.
                    }
                }
            }
            Ok(None) | Err(_) => {
                // Failed to read data, stop handling client.
                return Ok(DISCONNECT);
            }
        }
        interserver.close();
        self.handle_err(ProtocolError::InternalError, tr_id, DISCONNECT)
    }

    /// Handle Switchboard Session Answers.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 8. Session based Instant Messaging Protocol Details
    ///     > [8.4 Getting Invited to a Switchboard Session][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-8.4
    fn handle_ans(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: ANS TrID LocalUserHandle AuthResponseInfo SessionID
        // S: IRO TrID Participant# TotalParticipants UserHandle FriendlyName
        // S: ANS TrID OK

        let user_handle = command.parameters.get(0);
        let auth_response_info = command.parameters.get(1);
        let session_id = command.parameters.get(2);
        if user_handle.is_none() || auth_response_info.is_none() || session_id.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }

        let user_handle = user_handle.expect("No user handle in command.");
        let auth_response_info = auth_response_info.expect("No auth response info in command.");
        let session_id = session_id.expect("No session ID in command.");

        match User::get_from_user_handle(&user_handle, Arc::clone(&self.database)) {
            Ok(mut user) => {
                let mut found_cookie = false;
                if let Ok(cookies) = user.get_cookies("SESSION") {
                    for cookie in cookies {
                        let cookie_value = cookie
                            .get("cookie")
                            .expect("No cookie value for this cookie.");
                        let cookie_session = cookie
                            .get("session_id")
                            .expect("No session for this cookie");
                        if cookie_value == auth_response_info && session_id == cookie_session {
                            found_cookie = true;
                        }
                    }
                }

                if !found_cookie {
                    return self.handle_err(ProtocolError::AuthenticationFailed, tr_id, DISCONNECT);
                }

                if user.delete_cookie(auth_response_info).is_err() {
                    // Failed to delete session cookie.
                }
                self.user = Some(user);
            }
            Err(UserError::NoSuchUser) => {
                // Fail as internal error to avoid user guessing.
                return self.handle_err(ProtocolError::InternalError, tr_id, DISCONNECT);
            }
            Err(UserError::DatabaseConnectError) => {
                return self.handle_err(ProtocolError::DatabaseConnectError, tr_id, DISCONNECT);
            }
            Err(UserError::DatabaseError) => {
                return self.handle_err(ProtocolError::DatabaseError, tr_id, DISCONNECT);
            }
        }

        let sessions = self.sessions.read().expect("Lock sessions for read.");
        match sessions.get(session_id) {
            Some(session) => {
                let user = self.user.as_ref().expect("User not set.");
                if session.add_user(user, tr_id, self.client.clone()).is_ok() {
                    self.session = Some(session.clone());
                    Ok(KEEP_GOING)
                } else {
                    Ok(DISCONNECT)
                }
            }
            None => Ok(DISCONNECT),
        }
    }

    /// Handle Instant Messages.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 8. Session based Instant Messaging Protocol Details
    ///     > [8.7 Instant Messages][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-8.7
    fn handle_msg(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: MSG TrID [U | N | A] Length\r\nMessage
        // S: NAK TrID
        // S: ACK TrID

        let acknowledgement_mode = command.parameters.get(0);
        let payload_length = command.parameters.get(1);
        if acknowledgement_mode.is_none() || payload_length.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }

        let acknowledgement_mode =
            acknowledgement_mode.expect("No aknowledgement mode in command.");
        match acknowledgement_mode.as_str() {
            "U" | "N" | "A" => {}
            _ => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        }
        let payload_length = match payload_length
            .expect("No payload length in command.")
            .parse::<u64>()
        {
            Ok(length) => length,
            Err(_) => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        };

        let session = self.session.as_ref().expect("Session not set.");
        let user = self.user.as_ref().expect("User not set.");

        let payload = match self.client.read_payload(payload_length) {
            Ok(Some(payload)) => payload,
            Ok(None) | Err(_) => return Ok(DISCONNECT),
        };
        let message_sent_successfully = session.send_message(user, payload);

        match acknowledgement_mode.as_str() {
            "U" => {
                // Unacknowledged mode.
            }
            "N" => {
                // Negative-Acknowledgement-Only mode.
                if !message_sent_successfully {
                    let _ = self.client.write(format!("NAK {}", tr_id));
                }
            }
            "A" => {
                // Acknowledgement mode.
                if message_sent_successfully {
                    let _ = self.client.write(format!("ACK {}", tr_id));
                } else {
                    let _ = self.client.write(format!("NAK {}", tr_id));
                }
            }
            _ => {
                panic!(
                    "Tried to check an unknown acknowledgemnent mode: {}.",
                    acknowledgement_mode
                )
            }
        }

        Ok(KEEP_GOING)
    }

    /// Handle Client disconnection.
    fn handle_out(&mut self) {
        // S: OUT {StatusCode}

        let _ = self.client.write(format!("OUT"));

        match &mut self.user {
            Some(user) => {
                match &self.session {
                    Some(session) => {
                        if session.remove_user(user) {
                            // Session has no more participant, drop it.
                            let mut sessions =
                                self.sessions.write().expect("Lock sessions for write.");
                            sessions.remove(&session.id.to_string());
                        }
                    }
                    None => {}
                }
            }
            None => {}
        }
    }
}

impl Protocol for MSNP2Switchboard {
    fn handle(&mut self, command: &mut Command) -> HandleResult {
        // C: OUT
        if command.command_type == "OUT" {
            self.handle_out();
            return Ok(DISCONNECT);
        }

        if command.parameters.len() < 1 {
            // No TrID, disconnect.
            return Ok(DISCONNECT);
        }
        let tr_id = command.parameters.remove(0);

        match command.command_type.as_str() {
            "USR" => self.handle_usr(&tr_id, command), // 8.2 Switchboard Authentication
            "CAL" => self.handle_cal(&tr_id, command), // 8.3 Inviting Users to a Switchboard Session
            "ANS" => self.handle_ans(&tr_id, command), // 8.4 Getting Invited to a Switchboard Session
            "MSG" => self.handle_msg(&tr_id, command), // 8.7 Instant Messages
            _ => {
                println!("Unimplemented command : {:?}", command);
                self.handle_err(ProtocolError::SyntaxError, &tr_id, KEEP_GOING)
            }
        }
    }

    fn send_error_to_client(&mut self, error_type: u32, tr_id: &str) {
        let _ = self.client.write(format!("{} {}", error_type, tr_id));
    }
}
