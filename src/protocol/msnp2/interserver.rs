//! [`MSNP2InterServer`] MSN Inter-Server Protocol handling.
use crate::command::Command;
use crate::connection::Connection;
use crate::database::Database;
use crate::protocol::{HandleResult, Protocol, ProtocolError, DISCONNECT, KEEP_GOING};
use crate::server::IdentifiedClientList;
use crate::user::User;

use std::sync::Arc;

/// An MSNP2 Inter-Server Protocol handler.
pub struct MSNP2InterServer {
    /// Current MSN InterServer [`Connection`] being handled.
    client: Connection,
    /// Current MSN [`Database`] backend being used.
    database: Arc<Database>,
    /// Identified users on this Notification Server.
    users: IdentifiedClientList,
}

impl MSNP2InterServer {
    /// Creates an MSNP2 Inter-Server protocol handler.
    ///
    /// # Arguments
    /// * _client_: An MSN Inter-Server [`Connection`] to handle.
    /// * _database_: Current MSN [`Database`] backend being used.
    /// * _users_: Identified users on this Notification Server.
    pub fn new(
        client: Connection,
        database: Arc<Database>,
        users: IdentifiedClientList,
    ) -> MSNP2InterServer {
        MSNP2InterServer {
            client,
            database,
            users,
        }
    }

    /// Handle Client Invite to Switchboard.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 8. Session based Instant Messaging Protocol Details
    ///     > [8.3 Inviting Users to a Switchboard Session][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-8.3
    fn handle_cal(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: CAL CallingUserHandle CallingCustomUserName CalleeUserHandle SessionID
        // S: CAL ...

        let calling = command.parameters.get(0);
        let calling_custom_user_name = command.parameters.get(1);
        let callee = command.parameters.get(2);
        let session_id = command.parameters.get(3);

        if calling.is_none()
            || calling_custom_user_name.is_none()
            || callee.is_none()
            || session_id.is_none()
        {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }

        let calling = calling.expect("No calling user handle in command.");
        let calling_custom_user_name =
            calling_custom_user_name.expect("No calling custom user name in command.");
        let callee = callee.expect("No callee user handle in command.");
        let session_id = session_id.expect("No session ID in command.");

        match User::get_from_user_handle(&callee, Arc::clone(&self.database)) {
            Ok(mut callee) => {
                if callee.is_online() {
                    if let Ok(allowed) = callee.allows(&calling) {
                        if allowed {
                            if let Ok(cookie) = callee.add_cookie("SESSION", Some(session_id)) {
                                let users = self.users.read().expect("Lock users for read.");
                                if let Some(callee_client) = users.get(&callee.user_handle) {
                                    // S: RNG SessionID SwitchboardServerAddress
                                    //        SP AuthChallengeInfo
                                    //        CallingUserHandle CallingUserFriendlyName
                                    let _ = callee_client.clone().write(format!(
                                        "RNG {} messenger.hotmail.com:1866 CKI {} {} {}",
                                        session_id, cookie, calling, calling_custom_user_name
                                    ));
                                    let _ = self.client.write("CAL RINGING".to_string());
                                    return Ok(DISCONNECT);
                                }
                            }
                        }
                    }
                }
            }
            Err(_) => {
                // User does not exist, or failed to read user informations.
            }
        };
        self.handle_err(ProtocolError::InternalError, tr_id, DISCONNECT)
    }
}

impl Protocol for MSNP2InterServer {
    fn handle(&mut self, command: &mut Command) -> HandleResult {
        // C: OUT
        if command.command_type == "OUT" {
            let _ = self.client.write(format!("OUT"));
            return Ok(DISCONNECT);
        }

        match command.command_type.as_str() {
            "CAL" => self.handle_cal("0", command), // 8.3 Inviting Users to a Switchboard Session.
            _ => {
                println!("Unimplemented command : {:?}", command);
                self.handle_err(ProtocolError::SyntaxError, "0", KEEP_GOING)
            }
        }
    }

    fn send_error_to_client(&mut self, error_type: u32, tr_id: &str) {
        let _ = self.client.write(format!("{} {}", error_type, tr_id));
    }
}
