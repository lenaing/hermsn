//! [`MSNP2`] MSN Protocol handling.
use crate::command::Command;
use crate::connection::Connection;
use crate::database::Database;
use crate::hash;
use crate::protocol::{HandleResult, Protocol, ProtocolError, DISCONNECT, KEEP_GOING};
use crate::server::IdentifiedClientList;
use crate::user::{User, UserError};

mod interserver;
mod switchboard;
pub use interserver::MSNP2InterServer;
pub use switchboard::MSNP2Switchboard;

use std::sync::Arc;

/// MSNP2 authentication workflow states.
#[derive(PartialEq)]
enum AuthenticationState {
    Waiting,
    SentChallenge,
    Finalized,
}

/// An MSNP2 Protocol handler.
pub struct MSNP2 {
    /// Current client MSN [`Connection`] being handled.
    client: Connection,
    /// Current MSN [`Database`] backend being used.
    database: Arc<Database>,
    /// Client's associated MSN [`User`].
    user: Option<User>,
    /// Others Clients on the current Notification Server.
    users: IdentifiedClientList,
    /// Current client authentication state.
    authentication_state: AuthenticationState,
    /// Last authentication challenge salt sent.
    authentication_challenge_salt: Option<String>,
    /// Wether the initial change state was sent to client.
    sent_initial_chg: bool,
    /// Last member directory search results.
    last_search_results: Vec<String>,
}

impl MSNP2 {
    /// Creates an MSNP2 protocol handler.
    ///
    /// # Arguments
    /// * _client_: An MSN [`Connection`] to handle.
    /// * _database_: Current MSN [`Database`] backend being used.
    /// * _users_: Identified users on the current Notification Server.
    pub fn new(client: Connection, database: Arc<Database>, users: IdentifiedClientList) -> MSNP2 {
        MSNP2 {
            client: client,
            database: database,
            user: None,
            users: users,
            authentication_state: AuthenticationState::Waiting,
            authentication_challenge_salt: None,
            sent_initial_chg: false,
            last_search_results: vec![],
        }
    }

    //----------------------------------------------------------------------------------------------
    // Presence and State related handlers
    //----------------------------------------------------------------------------------------------

    /// Handle Server Policy information queries.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.2 Server Policy Information][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.2
    fn handle_inf(&mut self, tr_id: &str, _command: &Command) -> HandleResult {
        // C: INF TrID
        // S: INF TrID SP{,SP...}
        if self.client.write(format!("INF {} MD5", tr_id)).is_err() {
            return Ok(DISCONNECT);
        }
        Ok(KEEP_GOING)
    }

    /// Handle Authentication queries.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.3 Authentication][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.3
    fn handle_usr(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: USR TrID SP I{ AuthInitiateInfo}
        // S: USR TrID SP S{ AuthChallengeInfo}
        // C: USR TrID SP S{ AuthResponseInfo }
        // S: USR TrID OK UserHandle FriendlyName

        // User is already logged in through this connection.
        if self.authentication_state == AuthenticationState::Finalized {
            return self.handle_err(ProtocolError::AlreadyLoggedIn, tr_id, KEEP_GOING);
        }

        let security_package = command.parameters.get(0);
        let sequence = command.parameters.get(1);

        if security_package.is_none() || sequence.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }

        // MSNP2 only supports MD5 authentication challenge.
        if "MD5" != security_package.expect("No security package in command.") {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, DISCONNECT);
        }

        let mut error_type = ProtocolError::AuthenticationFailed;
        match sequence.expect("No sequence in command.").as_str() {
            "I" => {
                // Initialize authentication step.
                if self.authentication_state == AuthenticationState::Waiting {
                    if let Some(user_handle) = command.parameters.get(2) {
                        if User::is_user_handle_valid(user_handle) {
                            match User::get_from_user_handle(
                                &user_handle,
                                Arc::clone(&self.database),
                            ) {
                                Ok(user) => {
                                    if let Ok(salt) =
                                        hash::extract_salt_from_md5_password(&user.password)
                                    {
                                        if self
                                            .client
                                            .write(format!("USR {} MD5 S {}", tr_id, salt))
                                            .is_err()
                                        {
                                            return Ok(DISCONNECT);
                                        }
                                        self.authentication_state =
                                            AuthenticationState::SentChallenge;
                                        self.authentication_challenge_salt = Some(salt);
                                        self.user = Some(user);
                                        return Ok(KEEP_GOING);
                                    } else {
                                        error_type = ProtocolError::InternalError;
                                    }
                                }
                                Err(UserError::NoSuchUser) => {
                                    // Fail as internal error to avoid user guessing.
                                    error_type = ProtocolError::InternalError;
                                }
                                Err(UserError::DatabaseConnectError) => {
                                    error_type = ProtocolError::DatabaseConnectError;
                                }
                                Err(UserError::DatabaseError) => {
                                    error_type = ProtocolError::DatabaseError;
                                }
                            }
                        }
                    }
                }
            }
            "S" => {
                // Authentication challenge response step.
                if self.authentication_state == AuthenticationState::SentChallenge {
                    if let Some(challenge_response) = command.parameters.get(2) {
                        let user = self.user.as_ref().expect("User not logged in.");
                        if let Ok(hash) = hash::extract_hash_from_md5_password(&user.password) {
                            if challenge_response == &hash {
                                let mut users =
                                    self.users.write().expect("Lock users for modification.");

                                // Disconnect user if already connected from another location.
                                if let Some(old_user_connection) = users.get(&user.user_handle) {
                                    let mut old_user_connection = old_user_connection.clone();
                                    let _ = old_user_connection.write(format!("OUT OTH"));
                                    old_user_connection.close();
                                    users.remove(&user.user_handle);
                                }

                                if self
                                    .client
                                    .write(format!(
                                        "USR {} OK {} {}",
                                        tr_id, user.user_handle, user.custom_user_name
                                    ))
                                    .is_err()
                                {
                                    return Ok(DISCONNECT);
                                }
                                self.authentication_state = AuthenticationState::Finalized;
                                // Append user to identified users.
                                users.insert(user.user_handle.to_string(), self.client.clone());
                                return Ok(KEEP_GOING);
                            }
                        } else {
                            error_type = ProtocolError::InternalError;
                        }
                    }
                }
            }
            _ => {
                // Unknown sequence step.
            }
        };
        self.handle_err(error_type, tr_id, DISCONNECT)
    }

    /// Send a User Contact list.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 5. Protocol Conventions
    ///     > [5.7 User List Types][1]</cite>
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _list_type_: MSN Protocol List type.
    /// * _should_disconnect_: Wether to disconnect current user on error.
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-5.7
    fn send_list(&mut self, tr_id: &str, list_type: &str, should_disconnect: bool) -> HandleResult {
        let user = self.user.as_ref().expect("User not logged in.");

        let list = match list_type {
            "FL" => user.get_forward_list(),
            "AL" => user.get_allow_list(),
            "BL" => user.get_block_list(),
            "RL" => user.get_reverse_list(),
            _ => panic!("Tried to send an unknown list type: {}.", list_type),
        };

        match list {
            Ok(list) => {
                if list.is_empty() {
                    let _ = self
                        .client
                        .write(format!("LST {} {} {} 0 0", tr_id, list_type, user.serial));
                } else {
                    let total = list.len();
                    for (index, other_user) in list.iter().enumerate() {
                        let other_user_handle = other_user
                            .get("user_handle")
                            .expect("No user_handle for this user.");
                        let other_custom_user_name = match list_type {
                            "RL" => other_user_handle,
                            _ => other_user
                                .get("custom_user_name")
                                .expect("No custom_user_name for this user."),
                        };
                        let _ = self.client.write(format!(
                            "LST {} {} {} {} {} {} {}",
                            tr_id,
                            list_type,
                            user.serial,
                            index + 1,
                            total,
                            other_user_handle,
                            other_custom_user_name
                        ));
                    }
                }
                Ok(should_disconnect)
            }
            Err(UserError::DatabaseConnectError) => self.handle_err(
                ProtocolError::DatabaseConnectError,
                tr_id,
                should_disconnect,
            ),
            Err(UserError::NoSuchUser) | Err(UserError::DatabaseError) => {
                self.handle_err(ProtocolError::DatabaseError, tr_id, should_disconnect)
            }
        }
    }

    /// Handle User Properties Synchronization queries.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.5 Client User Property Synchronization][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.5
    fn handle_syn(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: SYN TrID Ser#
        // S: SYN TrID Ser#

        let client_serial = command.parameters.get(0);
        if client_serial.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, DISCONNECT);
        }
        let client_serial = match client_serial.expect("No serial in command.").parse::<u32>() {
            Ok(serial) => serial,
            Err(_) => return self.handle_err(ProtocolError::InvalidParameter, tr_id, DISCONNECT),
        };

        let user = self.user.as_ref().expect("User not logged in.");
        if self
            .client
            .write(format!("SYN {} {}", tr_id, user.serial))
            .is_err()
        {
            return Ok(DISCONNECT);
        }

        if client_serial != user.serial {
            // Reverse list prompting (GTC = GoT Contact ?)
            if self
                .client
                .write(format!("GTC {} {} {}", tr_id, user.serial, user.gtc))
                .is_err()
            {
                return Ok(DISCONNECT);
            }
            // Privacy policy (BLP = Block List Policy)
            if self
                .client
                .write(format!("BLP {} {} {}", tr_id, user.serial, user.blp))
                .is_err()
            {
                return Ok(DISCONNECT);
            }

            self.send_list(tr_id, "FL", DISCONNECT)?; // Forward List
            self.send_list(tr_id, "AL", DISCONNECT)?; // Allow List
            self.send_list(tr_id, "BL", DISCONNECT)?; // Block List
            self.send_list(tr_id, "RL", DISCONNECT)?; // Reverse List
        }

        Ok(KEEP_GOING)
    }

    /// Handle Client List check.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.6 List Retrieval And Property Management][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.6
    fn handle_lst(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: LST TrID LIST
        // S: LST TrID LIST Ser# Item# TtlItems UserHandle CustomUserName
        // or
        // S: LST TrID LIST Ser# 0 0

        let list = command.parameters.get(0);
        if list.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let list = list.expect("No list in command.");
        match list.as_str() {
            "FL" | "AL" | "BL" | "RL" => self.send_list(&tr_id, list, KEEP_GOING),
            _ => self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING),
        }
    }

    /// Handle Client Reverse List Prompting setting.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.6 List Retrieval And Property Management][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.6
    fn handle_gtc(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: GTC TrID [A | N]
        // S: GTC TrID Ser# [A | N]

        let gtc = command.parameters.get(0);
        if gtc.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let gtc = gtc.expect("No gtc in command.").as_str();
        match gtc {
            "A" | "N" => {}
            _ => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        }

        let user = self.user.as_mut().expect("User not logged in.");
        if gtc == user.gtc {
            return self.handle_err(ProtocolError::AlreadyInTheMode, tr_id, KEEP_GOING);
        }

        match user.set_gtc(gtc) {
            Ok(()) => {
                let _ = self
                    .client
                    .write(format!("GTC {} {} {}", tr_id, user.serial, gtc));
                Ok(KEEP_GOING)
            }
            Err(UserError::DatabaseConnectError) => {
                self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
            }
            Err(UserError::NoSuchUser) | Err(UserError::DatabaseError) => {
                self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
            }
        }
    }

    /// Handle Client Privacy Mode setting.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.6 List Retrieval And Property Management][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.6
    fn handle_blp(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: BLP TrID [AL | BL]
        // S: BLP TrID Ser# [AL | BL]

        let blp = command.parameters.get(0);
        if blp.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let blp = blp.expect("No blp in command.").as_str();
        match blp {
            "AL" | "BL" => {}
            _ => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        }

        let user = self.user.as_mut().expect("User not logged in.");
        if blp == user.blp {
            return self.handle_err(ProtocolError::AlreadyInTheMode, tr_id, KEEP_GOING);
        }

        match user.set_blp(blp) {
            Ok(()) => {
                let _ = self
                    .client
                    .write(format!("BLP {} {} {}", tr_id, user.serial, blp));
                return Ok(KEEP_GOING);
            }
            Err(UserError::DatabaseConnectError) => {
                self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
            }
            Err(UserError::NoSuchUser) | Err(UserError::DatabaseError) => {
                self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
            }
        }
    }

    /// Handle Client States change queries.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.7 Client States][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.7
    fn handle_chg(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: CHG TrID State
        // S: CHG TrID State

        let state = command.parameters.get(0);
        if state.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let state = state.expect("No state in command.");
        match state.as_str() {
            "NLN" | "HDN" | "BSY" | "IDL" | "BRB" | "AWY" | "PHN" | "LUN" => {}
            "FLN" | _ => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        }

        let user = self.user.as_mut().expect("User not logged in.");
        let previous_state = user.state.to_string();

        // Change user state.
        match user.set_state(state) {
            Ok(()) => {
                let _ = self.client.write(format!("CHG {} {}", tr_id, state));
            }
            Err(UserError::DatabaseConnectError) => {
                return self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING);
            }
            Err(UserError::NoSuchUser) | Err(UserError::DatabaseError) => {
                return self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING);
            }
        }

        // Inform the current user of her online contacts on initial CHG command.
        if !self.sent_initial_chg {
            if let Ok(forward_list) = user.get_forward_list() {
                let users = self.users.read().expect("Lock users for read.");
                for other_user in forward_list {
                    let other_user_handle = other_user
                        .get("user_handle")
                        .expect("No user_handle for this user.");
                    if users.contains_key(other_user_handle) {
                        let other_custom_user_name = other_user
                            .get("custom_user_name")
                            .expect("No custom_user_name for this user.");
                        let other_state = other_user.get("state").expect("No state for this user.");
                        match other_state.as_str() {
                            "FLN" | "HDN" => {}
                            _ => {
                                if let Ok(other_user) = User::get_from_user_handle(
                                    other_user_handle,
                                    Arc::clone(&self.database),
                                ) {
                                    if let Ok(allowed) = other_user.allows(&user.user_handle) {
                                        if allowed {
                                            let _ = self.client.write(format!(
                                                "ILN {} {} {} {}",
                                                tr_id,
                                                other_state,
                                                other_user_handle,
                                                other_custom_user_name
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                // Failed to get user's forward list.
            }
            self.sent_initial_chg = true;
        }

        self.notify_state_change(&previous_state);
        Ok(KEEP_GOING)
    }

    /// Notify user's contacts from a state change.
    ///
    /// # Arguments
    /// * _previous_state_: Previous user's state.
    fn notify_state_change(&mut self, user_previous_state: &str) {
        // Inform the online contacts of the current user presence.
        let user = self.user.as_ref().expect("User not logged in.");

        if let Ok(reverse_list) = user.get_reverse_list() {
            let users = self.users.read().expect("Lock users for read.");
            for other_user in reverse_list {
                let other_user_handle = other_user
                    .get("user_handle")
                    .expect("No user_handle for this user.");
                if let Ok(allowed) = user.allows(&other_user_handle) {
                    if allowed {
                        if users.contains_key(other_user_handle) {
                            let other_state =
                                other_user.get("state").expect("No state for this user.");
                            match other_state.as_str() {
                                "FLN" => {}
                                _ => {
                                    if let Some(other_client) = users.get(other_user_handle) {
                                        let mut other_client = other_client.clone();
                                        if user.is_online() {
                                            let _ = other_client.write(format!(
                                                "NLN {} {} {}",
                                                user.state, user.user_handle, user.custom_user_name
                                            ));
                                        } else if user_previous_state != "FLN"
                                            && user_previous_state != "HDN"
                                        {
                                            // User is hiding or going offline and wasn't previously.
                                            let _ = other_client
                                                .write(format!("FLN {}", user.user_handle));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            // Failed to get user's reverse list.
        }
    }

    /// Notify user's contact of a reverse list update.
    /// Happens after addition or removal of contact on the user's forward list.
    ///
    /// # Arguments
    /// * _operation_: "ADD" or "REM" contact.
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _other_user_handle_: This user's contact.
    fn notify_reverse_list_update(
        &mut self,
        operation: &str,
        tr_id: &str,
        other_user_handle: &str,
    ) -> HandleResult {
        match User::get_from_user_handle(other_user_handle, Arc::clone(&self.database)) {
            Ok(mut other_user) => {
                match other_user.increment_serial() {
                    Ok(()) => {
                        // Send the contact an ADD/REM RL of the current user.

                        // Unfortunately, there may be a race condition here when two users add or
                        // delete this same contact at the exact same time: We may send a previous
                        // serial with our notification.
                        // The client may not interpret the command if the serial is lower than its
                        // current cache one, so a contact may stay in/be absent from their reverse
                        // list until a full sync is done later.

                        let user = self.user.as_ref().expect("User not logged in.");

                        // Only if user is online and if the user allow the contact, to avoid
                        // revealing that the user is currently hiding.
                        if let Ok(other_allowed) = user.allows(other_user_handle) {
                            if user.is_online() && other_allowed {
                                let users = self.users.read().expect("Lock users for read.");
                                if let Some(other_client) = users.get(other_user_handle) {
                                    let _ = other_client.clone().write(format!(
                                        "{} RL 0 {} {} {}",
                                        operation,
                                        other_user.serial,
                                        user.user_handle,
                                        user.custom_user_name
                                    ));
                                }
                            }
                        }

                        if operation == "ADD" {
                            // Send the current user an ILN of the contact.
                            if let Ok(allowed) = other_user.allows(&user.user_handle) {
                                if allowed {
                                    let _ = self.client.write(format!(
                                        "ILN {} {} {} {}",
                                        tr_id,
                                        other_user.state,
                                        other_user.user_handle,
                                        other_user.custom_user_name
                                    ));
                                }
                            }
                        }
                        Ok(KEEP_GOING)
                    }
                    Err(_) => {
                        // Failed to update contact's serial.
                        Ok(KEEP_GOING)
                    }
                }
            }
            Err(user_error) => match user_error {
                UserError::DatabaseConnectError => {
                    self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
                }
                UserError::DatabaseError => {
                    self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
                }
                UserError::NoSuchUser => Ok(KEEP_GOING),
            },
        }
    }

    /// Handle Client List Addition.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.8 List Modifications][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.8
    fn handle_add(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: ADD TrID LIST UserHandle CustomUserName
        // S: ADD TrID LIST ser# UserHandle CustomUserName

        let list = command.parameters.get(0);
        let other_user_handle = command.parameters.get(1);
        let other_custom_user_name = command.parameters.get(2);
        if list.is_none() || other_user_handle.is_none() || other_custom_user_name.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let list = list.expect("No list in command.");
        match list.as_str() {
            "FL" | "AL" | "BL" => {}
            "RL" | _ => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        }

        let user = self.user.as_mut().expect("User not logged in.");
        let other_user_handle = other_user_handle.expect("No user handle in command.");
        let other_custom_user_name =
            other_custom_user_name.expect("No custom user name in command.");

        if !User::is_user_handle_valid(&other_user_handle) {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
        }

        match user.add_to_list(list, other_user_handle, other_custom_user_name) {
            Ok(()) => {
                let _ = self.client.write(format!(
                    "ADD {} {} {} {} {}",
                    tr_id, list, user.serial, other_user_handle, other_custom_user_name
                ));

                if list == "FL" {
                    match self.notify_reverse_list_update("ADD", tr_id, &other_user_handle) {
                        _ => {
                            // Don't choke on RL notification errors.
                        }
                    }
                }

                Ok(KEEP_GOING)
            }
            Err(user_error) => match user_error {
                UserError::DatabaseConnectError => {
                    self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
                }
                UserError::DatabaseError => {
                    self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
                }
                UserError::NoSuchUser => Ok(KEEP_GOING),
            },
        }
    }

    /// Handle Client List Removal.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.8 List Modifications][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.8
    fn handle_rem(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: REM TrID LIST UserHandle
        // S: REM TrID LIST ser# UserHandle

        let list = command.parameters.get(0);
        let other_user_handle = command.parameters.get(1);
        if list.is_none() || other_user_handle.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let list = list.expect("No list in command.");
        match list.as_str() {
            "FL" | "AL" | "BL" => {}
            "RL" | _ => {
                return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
            }
        }

        let user = self.user.as_mut().expect("User not logged in.");
        let other_user_handle = other_user_handle.expect("No user handle in command.");

        if !User::is_user_handle_valid(&other_user_handle) {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
        }

        match user.remove_from_list(list, other_user_handle) {
            Ok(()) => {
                let _ = self.client.write(format!(
                    "REM {} {} {} {}",
                    tr_id, list, user.serial, other_user_handle
                ));

                if list == "FL" {
                    match self.notify_reverse_list_update("REM", tr_id, &other_user_handle) {
                        _ => {
                            // Don't choke on RL notification errors.
                        }
                    }
                }

                if list == "BL" || list == "AL" {
                    // Unblock or block contact if removed from Block or Allow Lists.
                    let user = self.user.as_ref().expect("User not logged in.");
                    if user.is_online() {
                        let users = self.users.read().expect("Lock users for read.");
                        if let Some(other_client) = users.get(other_user_handle) {
                            let state_line = if list == "BL" {
                                // Removal from Block List: Notify this user's contact that the
                                // user is now online.
                                format!(
                                    "NLN {} {} {}",
                                    user.state, user.user_handle, user.custom_user_name
                                )
                            } else {
                                // Removal from Allow List: Notify this user's contact that the
                                // user is now offline.
                                format!("FLN {}", user.user_handle)
                            };
                            let _ = other_client.clone().write(state_line);
                        }
                    }
                }
                return Ok(KEEP_GOING);
            }
            Err(user_error) => match user_error {
                UserError::DatabaseConnectError => {
                    self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
                }
                UserError::DatabaseError => {
                    self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
                }
                UserError::NoSuchUser => Ok(KEEP_GOING),
            },
        }
    }

    /// Handle Client disconnection.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 7. Presence and State Protocol Details
    ///     > [7.10 Connection Close][1]</cite>
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-7.10
    fn handle_out(&mut self) {
        // S: OUT {StatusCode}
        let _ = self.client.write(format!("OUT"));
        match &mut self.user {
            Some(user) => {
                let user_handle = user.user_handle.to_string();
                let previous_state = user.state.to_string();

                // Update state in database.
                match user.set_state("FLN") {
                    _ => {
                        // Don't choke on state update failure.
                    }
                }

                // Ensure to set user offline.
                user.state = "FLN".to_string();
                self.notify_state_change(&previous_state);

                // Remove this user from this NS server identified users.
                self.users
                    .write()
                    .expect("Lock users for deletion.")
                    .remove(&user_handle);
            }
            None => {}
        }
    }

    /// Handle custom user name changes.
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    fn handle_rea(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: REA TrID UserHandle CustomUserName
        // S: REA TrID ser# UserHandle CustomUserName

        let user_handle = command.parameters.get(0);
        let custom_user_name = command.parameters.get(1);
        if user_handle.is_none() || custom_user_name.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let user_handle = user_handle.expect("No user handle in command.");
        let custom_user_name = custom_user_name.expect("No custom user name in command.");

        let user = self.user.as_mut().expect("User not logged in.");
        match user.rename(user_handle, custom_user_name) {
            Ok(()) => {
                let _ = self.client.write(format!(
                    "REA {} {} {} {}",
                    tr_id, user.serial, user_handle, custom_user_name
                ));
                if user.user_handle == user_handle.to_string() {
                    let current_state = user.state.to_string();
                    self.notify_state_change(&current_state);
                }
                Ok(KEEP_GOING)
            }
            Err(user_error) => match user_error {
                UserError::DatabaseConnectError => {
                    self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
                }
                UserError::NoSuchUser | UserError::DatabaseError => {
                    self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
                }
            },
        }
    }

    //----------------------------------------------------------------------------------------------
    // Instant Messaging related handlers
    //----------------------------------------------------------------------------------------------

    /// Handle Client Referral to Switchboard.
    ///
    /// > <cite>MSN Messenger Service 1.0 Protocol
    ///     > 8. Session based Instant Messaging Protocol Details
    ///     > [8.1 Referral to Switchboard][1]</cite>
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    ///
    /// [1]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00#section-8.1
    fn handle_xfr(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: XFR TrID SB
        // S: XFR TrID SB Address SP AuthChallengeInfo

        let referral_type = command.parameters.get(0);
        if referral_type.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let referral_type = referral_type.expect("No referral type in command.");
        if referral_type != "SB" {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
        }

        let user = self.user.as_mut().expect("User not logged in.");
        if !user.is_online() {
            return self.handle_err(ProtocolError::AuthenticationFailed, tr_id, KEEP_GOING);
        }
        match user.add_cookie("CONNECT", None) {
            Ok(cookie) => {
                let _ = self.client.write(format!(
                    "XFR {} SB messenger.hotmail.com:1866 CKI {}",
                    tr_id, cookie
                ));
                Ok(KEEP_GOING)
            }
            Err(user_error) => match user_error {
                UserError::DatabaseConnectError => {
                    self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
                }
                UserError::NoSuchUser | UserError::DatabaseError => {
                    self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
                }
            },
        }
    }

    //----------------------------------------------------------------------------------------------
    // Directory related handlers
    //----------------------------------------------------------------------------------------------

    /// Handle Member Directory Search.
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    fn handle_fnd(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: FND TrID fname=FirstName lname=LastName city=City state=State country=Country
        // S: FND TrID Item# fname=FirstName lname=LastName city=City state=State country=Country
        // or
        // S: FND TrID 0 0

        let first_name = command.parameters.get(0);
        let last_name = command.parameters.get(1);
        let city = command.parameters.get(2);
        let state = command.parameters.get(3);
        let country = command.parameters.get(4);

        if first_name.is_none()
            || last_name.is_none()
            || city.is_none()
            || state.is_none()
            || country.is_none()
        {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }

        let first_name_components: Vec<String> = first_name
            .expect("No first name in command")
            .splitn(2, "=")
            .map(|s| s.to_string())
            .collect();
        let last_name_components: Vec<String> = last_name
            .expect("No last name in command")
            .splitn(2, "=")
            .map(|s| s.to_string())
            .collect();
        let city_components: Vec<String> = city
            .expect("No city in command")
            .splitn(2, "=")
            .map(|s| s.to_string())
            .collect();
        let state_components: Vec<String> = state
            .expect("No state in command")
            .splitn(2, "=")
            .map(|s| s.to_string())
            .collect();
        let country_components: Vec<String> = country
            .expect("No country in command")
            .splitn(2, "=")
            .map(|s| s.to_string())
            .collect();

        if first_name_components.len() < 2
            || last_name_components.len() < 2
            || city_components.len() < 2
            || state_components.len() < 2
            || country_components.len() < 2
        {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
        }

        let first_name = first_name_components
            .get(1)
            .expect("No first name specified");
        let last_name = last_name_components.get(1).expect("No last name specified");
        let city = city_components.get(1).expect("No city specified");
        let state = state_components.get(1).expect("No state specified");
        let country = country_components.get(1).expect("No country specified");

        if first_name == "*" || last_name == "*" {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
        }

        match User::search(
            Arc::clone(&self.database),
            first_name,
            last_name,
            city,
            state,
            country,
        ) {
            Ok(search_results) => {
                if search_results.is_empty() {
                    let _ = self.client.write(format!("FND {} 0 0", tr_id));
                } else {
                    let total = search_results.len();
                    let mut last_search_results = vec![];
                    for (index, user) in search_results.iter().enumerate() {
                        let user_handle = user
                            .get("user_handle")
                            .expect("No user_handle for this user.");
                        last_search_results.push(user_handle.to_string());
                        let first_name = user
                            .get("first_name")
                            .expect("No first_name for this user.");
                        let last_name = user.get("last_name").expect("No last_name for this user.");
                        let mut result = format!(
                            "FND {} {} {} fname={} lname={}",
                            tr_id,
                            index + 1,
                            total,
                            first_name,
                            last_name
                        );
                        if let Some(city) = user.get("city") {
                            result = format!("{} city={}", result, city);
                        }
                        if let Some(state) = user.get("state") {
                            result = format!("{} state={}", result, state);
                        }
                        if let Some(country) = user.get("country") {
                            result = format!("{} country={}", result, country);
                        }
                        let _ = self.client.write(result);
                    }
                    self.last_search_results = last_search_results;
                }
                Ok(KEEP_GOING)
            }
            Err(user_error) => match user_error {
                UserError::DatabaseConnectError => {
                    self.handle_err(ProtocolError::DatabaseConnectError, tr_id, KEEP_GOING)
                }
                UserError::NoSuchUser | UserError::DatabaseError => {
                    self.handle_err(ProtocolError::DatabaseError, tr_id, KEEP_GOING)
                }
            },
        }
    }

    /// Handle Member Invitation.
    ///
    /// # Arguments
    /// * _tr_id_: Client [`Command`]'s Transaction ID.
    /// * _command_: Client [`Command`].
    fn handle_snd(&mut self, tr_id: &str, command: &Command) -> HandleResult {
        // C: SND TrID UserHandle
        // or
        // C: SND TrID SearchID
        // S: SND TrID OK

        let user_handle = command.parameters.get(0);
        if user_handle.is_none() {
            return self.handle_err(ProtocolError::RequiredFieldsMissing, tr_id, KEEP_GOING);
        }
        let mut user_handle = user_handle.expect("No user handle or search index in command.");

        // Convert search index to user handle.
        match user_handle.parse::<usize>() {
            Ok(search_index) => {
                user_handle = match self.last_search_results.get(search_index - 1) {
                    Some(user_handle) => user_handle,
                    None => {
                        return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
                    }
                }
            }
            Err(_) => {}
        };

        if !User::is_user_handle_valid(user_handle) {
            return self.handle_err(ProtocolError::InvalidParameter, tr_id, KEEP_GOING);
        }

        println!(
            "Unimplemented: Should send an email invitation to {} on behalf of {}.",
            user_handle,
            self.user.as_ref().expect("User not logged in").user_handle
        );
        let _ = self.client.write(format!("SND {} OK", tr_id));
        Ok(KEEP_GOING)
    }
}

impl Protocol for MSNP2 {
    fn handle(&mut self, command: &mut Command) -> HandleResult {
        // C: OUT
        if command.command_type == "OUT" {
            self.handle_out(); // 7.10 Connection Close.
            return Ok(DISCONNECT);
        }

        if command.parameters.len() < 1 {
            // No TrID, disconnect.
            return Ok(DISCONNECT);
        }
        let tr_id = command.parameters.remove(0);

        match command.command_type.as_str() {
            "INF" => self.handle_inf(&tr_id, command), // 7.2 Server Policy Information.
            "USR" => self.handle_usr(&tr_id, command), // 7.3 Authentication.
            "SYN" => self.handle_syn(&tr_id, command), // 7.5 Client User Property Synchronization.
            "LST" => self.handle_lst(&tr_id, command), // 7.6 List Retrieval And Property Management.
            "GTC" => self.handle_gtc(&tr_id, command), // 7.6 List Retrieval And Property Management.
            "BLP" => self.handle_blp(&tr_id, command), // 7.6 List Retrieval And Property Management.
            "CHG" => self.handle_chg(&tr_id, command), // 7.7 Client States.
            "ADD" => self.handle_add(&tr_id, command), // 7.8 List Modifications.
            "REM" => self.handle_rem(&tr_id, command), // 7.8 List Modifications.
            "XFR" => self.handle_xfr(&tr_id, command), // 8.1 Referral to Switchboard.
            // The following commands aren't documented in the original IETF Draft.
            "REA" => self.handle_rea(&tr_id, command), // Custom user name change.
            "FND" => self.handle_fnd(&tr_id, command), // Directory search.
            "SND" => self.handle_snd(&tr_id, command), // Send invitation.
            _ => {
                println!("Unimplemented command: {:?}", command);
                self.handle_err(ProtocolError::SyntaxError, &tr_id, KEEP_GOING)
            }
        }
    }

    fn send_error_to_client(&mut self, error_type: u32, tr_id: &str) {
        let _ = self.client.write(format!("{} {}", error_type, tr_id));
    }
}
