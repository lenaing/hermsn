//! MSN Protocol implementations.
use crate::command::Command;

mod msnp2;
pub use msnp2::MSNP2InterServer;
pub use msnp2::MSNP2Switchboard;
pub use msnp2::MSNP2;

/// A protocol error that can occur while handling an MSN [`Command`].
#[derive(Debug, Clone, Copy)]
pub enum ProtocolError {
    SyntaxError = 200,
    InvalidParameter = 201,
    AlreadyLoggedIn = 207,
    InvalidUsername = 208,
    AlreadyInTheMode = 218,
    RequiredFieldsMissing = 300,
    InternalError = 500,
    DatabaseError = 501,
    PeerNSIsDown = 602,
    DatabaseConnectError = 603,
    AuthenticationFailed = 911,
}

/// Should the user be disconnected as a result of her last command.
pub const DISCONNECT: bool = true;
/// Should the user stay connected as a result of her last command.
pub const KEEP_GOING: bool = false;

/// Result about handling an MSN [`Command`].
/// Either `Ok(should_disconnect)` on command successfully handled or
/// `Err((ProtocolError, tr_id, should_disconnect))` if failure.
///
/// * _should_disconnect_: If client should be disconnected from the server.
/// * _error_type_: An MSN [`ProtocolError`].
/// * _tr_id_: Client [`Command`]'s Transaction ID that encountered the error.
pub type HandleResult = Result<bool, (ProtocolError, String, bool)>;

/// Trait to implement an MSN Protocol.
pub trait Protocol {
    /// Handle an MSN [`Command`].
    ///
    /// # Arguments
    /// * _command_: An MSN [`Command`] to handle.
    ///
    /// Return `Ok(bool)` as `true` if client should be disconnected, or `false` otherwise.
    /// `Err<ProtocolError>` if an error occured while handling the command.
    fn handle(&mut self, command: &mut Command) -> HandleResult;

    /// Send a protocol error to the client.
    ///
    /// # Arguments
    /// * _error_type_: An MSN [`ProtocolError`]'s 3 digit decimal number.
    /// * _tr_id_: Client [`Command`]'s Transaction ID that encountered the error.
    fn send_error_to_client(&mut self, error_type: u32, tr_id: &str);

    /// Handle an MSN [`Command`] failure.
    ///
    /// # Arguments
    /// * _error_type_: An MSN [`ProtocolError`].
    /// * _tr_id_: Client [`Command`]'s Transaction ID that encountered the error.
    /// * _should_disconnect_: If client should be disconnected from the server.
    fn handle_err(
        &mut self,
        error_type: ProtocolError,
        tr_id: &str,
        should_disconnect: bool,
    ) -> HandleResult {
        self.send_error_to_client(error_type as u32, tr_id);
        Err((error_type, tr_id.to_string(), should_disconnect))
    }
}
