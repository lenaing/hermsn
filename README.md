# hermsn

A Rust library that provide tools to run an [MSN Messenger][1] service.

## Security

Please be warned that **everything** in the MSN Messenger Service protocol traffic (including
presence state, notifications, discussions and so on) is sent in **cleartext** over the wire.

**Do not communicate sensitive information through it.**

### Passwords

Passwords are never stored in clear text in the user database.

However, the _MSNP2_ version of the protocol uses an MD5 authentication mecanism. Even though
_hermsn_ expects the users passwords to be hashed and salted, it requires to store the salt
alongside the hashed password to send it to the client as a challenge during the authentication
phase.

As a cryptographic hashing function, MD5 is only computed in a way and can not be reversed.
Nevertheless, MD5 is *vulnerable to collision attacks*.

**Use a throw away password that you do not use for anything else to register to this service.**

## Quickstart Examples

### Create the server database

At the moment, only SQLite is supported.

```shell
# Create the database
sqlite3 hermsn.db < database/create_db.sql
# Add a user
./database/add_user.sh
User handle: example@hotmail.com
User password:
User First Name: Example
User Last Name: Elpmaxe
User Country: US
User State: CA
User City: Pasadena
Will add the following user to the database...
User handle: example@hotmail.com
User hashed password: 39c669cb8c48a382dcf6c70d3962597f
User name: Example Elpmaxe
User country/state/city: US/CA/Pasadena
Continue? (y/n): y
```

You will need at least 2 users to try _hermsn_ !

### Launch the server components

You will need to launch all server components to have a fully working _hermsn_ service. All
components should run on the same server and will use the following ports:

| Component           | Ports |
|---------------------|-------|
| Dispatch Server     | 1863  |
| Notification Server | 1864, 1865 (Inter-Server communication) |
| Switchboard Server  | 1866  |

#### Dispatch Server

```shell
cargo run --example hermsn-ds
```

#### Notification Server

```shell
cargo run --example hermsn-ns
```

#### Switchboard Server

```shell
cargo run --example hermsn-ss
```

### Configure your client

Install a compatible client and modify your hosts file to make `messenger.hotmail.com` point
to your server IP.

Connect. Have fun.

## Compatibility

_hermsn_ currently implements only the _MSNP2_ protocol.

The implementation of the protocol is mostly based on the original
[MSN Messenger Service 1.0 Protocol IETF Draft][2] published by Microsoft a few weeks after the
MSN Messenger Service was officially launched in July of 1999.

### Clients

Here is a list of clients known to work with _hermsn_.

| Client                              | Version  | Release Date | Protocol |
|-------------------------------------|----------|--------------|----------|
| Microsoft MSN Messenger Service 1.0 | 1.0.0863 | 1999/07/17   | MSNP2    |

## Known issues

### URL commands are not implemented

URL commands are used to redirect clients to different services of the network.
Some examples are:

* The `PERSON` URL should redirect to a service enabling users to modify their personnal profile.
* The `INBOX` and `COMPOSE` URL should redirect to a mail service.

Historically, this services were provided by the Microsoft Network
([MSN](https://en.wikipedia.org/wiki/MSN)) through msn.com and
[hotmail.com](https://en.wikipedia.org/wiki/Outlook.com).

Implementing a full network service is out of this project scope, and URL customisation feature
is not planned yet.

### Invitation emails aren't sent even if the notification server says otherwise

This would require _hermsn_ to be connected to a mail service that would allow to send emails
on behalf of users of the service. Implementation of this feature is not planned yet.

[1]: https://en.wikipedia.org/wiki/Windows_Live_Messenger#MSN_Messenger_1.0%E2%80%937.5_(1999%E2%80%932005)
[2]: https://tools.ietf.org/html/draft-movva-msn-messenger-protocol-00