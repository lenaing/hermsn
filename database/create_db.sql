BEGIN TRANSACTION;

-- Users' state
CREATE TABLE IF NOT EXISTS "users" (
	"user_handle"	    TEXT,
	"custom_user_name"	TEXT,
	"password"	    	TEXT,
	"gtc"	            TEXT,
	"blp"	            TEXT,
	"serial"	        INTEGER,
	"state"	            TEXT,
	PRIMARY KEY("user_handle")
);

-- Users' forward/reverse lists
CREATE TABLE IF NOT EXISTS "forward_list" (
	"user_handle"	        TEXT,
	"contact_user_handle"	TEXT,
	"custom_user_name"	    TEXT
);

-- Users' allow lists
CREATE TABLE IF NOT EXISTS "allow_list" (
	"user_handle"	        TEXT,
	"contact_user_handle"	TEXT,
	"custom_user_name"	    TEXT
);

-- Users' block lists
CREATE TABLE IF NOT EXISTS "block_list" (
	"user_handle"	        TEXT,
	"contact_user_handle"	TEXT,
	"custom_user_name"	    TEXT
);

-- Service cookies
-- Used to secure Notification and Switchboard servers referrals
CREATE TABLE IF NOT EXISTS "cookies" (
	"cookie_type"	TEXT,
	"user_handle"	TEXT,
	"cookie"	    TEXT,
	"session_id"	INTEGER,
	"creation_time" TEXT
);

-- Members directory
CREATE TABLE IF NOT EXISTS "directory" (
	"user_handle"	TEXT,
	"first_name"	TEXT,
	"last_name"	    TEXT,
	"country"	    TEXT,
	"state"	        TEXT,
	"city"	        TEXT
);

COMMIT;