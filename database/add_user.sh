#!/bin/sh
printf "User handle: "
read user_handle

case $user_handle in
    *@hotmail.com) ;;
    *) echo "User handle must have the @hotmail.com domain for MSNP2."
        exit 1;;
esac

stty -echo
printf "User password: "
read user_password
stty echo
printf "\n"

if [ -z "${user_password}" ]
then
    echo "User password should not be empty."
    exit 1
fi

salt="$(openssl rand -hex 16)"
password_hash="$(echo -n "${salt}${user_password}"|openssl md5|cut -d' ' -f2)"

printf "User First Name: "
read user_first_name
printf "User Last Name: "
read user_last_name
printf "User Country: "
read user_country
printf "User State: "
read user_state
printf "User City: "
read user_city


echo "Will add the following user to the database..."
echo "User handle: ${user_handle}"
echo "User hashed password: ${password_hash}"

if [ -z "${user_first_name}" -o -z "${user_last_name}" ]
then
    echo "User won't be added to members directory"
else
    echo "User name: ${user_first_name} ${user_last_name}"
    echo "User country/state/city: ${user_country}/${user_state}/${user_city}"

    if [ -z "${user_country}" ]; then user_country="NULL"; else user_country="'${user_country}'"; fi
    if [ -z "${user_state}" ]; then user_state="NULL"; else user_state="'${user_state}'"; fi
    if [ -z "${user_city}" ]; then user_city="NULL"; else user_city="'${user_city}'"; fi
fi

printf "Continue? (y/n): "
read choice
case "$choice" in
    y|Y);;
    *) exit 0
esac

echo "INSERT INTO users (user_handle, custom_user_name, password, gtc, blp, serial, state)
      VALUES ('${user_handle}', '${user_handle}', '\$msnp\$${salt}\$${password_hash}',
              'A', 'AL', 0, 'FLN');" | sqlite3 hermsn.db

if [ ! -z "${user_first_name}" -a ! -z "${user_last_name}" ]
then
    echo "INSERT INTO directory (user_handle, first_name, last_name, country, state, city)
          VALUES ('${user_handle}', '${user_first_name}', '${user_last_name}',
                   ${user_country}, ${user_state}, ${user_city});" | sqlite3 hermsn.db
fi
