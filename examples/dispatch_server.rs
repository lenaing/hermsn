extern crate ctrlc;
extern crate hermsn;

use hermsn::DispatchServer;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::{thread, time};

fn main() {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    println!("Starting demo Dispatch Server!");
    let ds = DispatchServer::start("0.0.0.0:1863", &vec!["MSNP2".to_string()]);
    while running.load(Ordering::SeqCst) {
        thread::sleep(time::Duration::from_millis(10));
    }
    println!("Exiting...");
    ds.stop();
}
